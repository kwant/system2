# -*- coding: utf-8 -*-

"""Build a system with a wallpaper group symmetry."""

import itertools as it

import numpy as np

import wallpaper_group as wpg
import wallpaper_system as wps


def make_system(sym, onsites, hoppings, is_hermitian=True):
    """Make a new system.

    Parameters
    ----------
    sym : wallpaper_group.Symmetry
    onsites : dict: Site -> array
        Onsite Hamiltonian matrices, sites are indexed by a tuple of integers
    hoppings : dict: (Site, Site) -> array
        Hopping Hamilotnian matrices.
    is_hermitian : bool, default: True

    Returns
    -------
    wallpaper_system.System
    """

    # Find dimensionality of the system by number of coordinates of sites
    sites = list(onsites.keys())
    d = len(sites[0].tag)
    if not all([len(s) == d for s in sites]):
        raise ValueError('All sites must have the same number of coordinates.')

    onsites = {site: np.atleast_2d(val) for site, val in onsites.items()}
    hoppings = {hop: np.atleast_2d(val) for hop, val in hoppings.items()}

    # TODO: allow sites from different lattices
    lat = sites[0].family
    if any(site.family != lat for site in onsites):
        raise TypeError('Currently all sites must be from the same lattice')

    if any(site.family != lat
           for site in it.chain.from_iterable(hoppings.keys())):
        raise TypeError('Currently all sites must be from the same lattice')

    # Find number of orbitals on each site
    rep_shape = lat.unitary_representation(lat.sym.id).shape
    if not all([val.shape == rep_shape for val in onsites.values()]):
        raise ValueError('All Hamiltonian terms must have the same shape.')
    if not all([val.shape == rep_shape for val in hoppings.values()]):
        raise ValueError('All Hamiltonian terms must have the same shape.')

    # Reduce system to the fundamental domain
    fd_subgroups, site_map = wpg.fundamental_domain(lat,
                                                    [s.tag for s in sites])
    fd_hops, hop_map = wpg.fundamental_hoppings(list(hoppings), fd_subgroups,
                                                is_hermitian=is_hermitian)

    def act(g, site):
        lat, tag = site
        return lat(*lat.act(g, tag))

    # Transform the values
    U = lat.unitary_representation
    fd_onsites = {}
    for fd_site, g in site_map.items():
        original_site = act(g, fd_site)
        H = onsites[original_site]
        fd_onsites[fd_site] = U(g.inv()) @ H @ U(g)

    fd_hoppings = {}
    for fd_hopping, (g, conjugate) in hop_map.items():
        a, b, h = fd_hopping
        ap, bp = (act(g, a), act(g * h, b))
        if conjugate:
            ap, bp = bp, ap
        H = hoppings[ap, bp]
        if conjugate:
            H = H.conjugate().transpose()
        fd_hoppings[fd_hopping] = U(g.inv()) @ H @ U(g)

    return wps.System(sym, fd_subgroups, fd_onsites, fd_hoppings, U)
