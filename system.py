# Copyright 2011-2016 Kwant authors.
#
# This file is part of Kwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of Kwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.

"""Low-level interface of systems and symmetries"""

import abc
import operator
from functools import reduce, total_ordering

import tinyarray as ta
import kwant


class GroupElement(tuple):
    """Abstract group element for low-level symmetries.

    Attributes
    ----------
    sym: Symmetry
        The symmetry group this element belongs to.
    t : array of int, or None
        The translational part of the symmetry, expressed in the basis
        of 'sym.translations'.
    r : object, or None
        The point-group part of the symmetry. If 'None' then the group
        element has no rotational part.
    """
    __slots__ = ()

    sym = property(operator.itemgetter(0),
                   doc="The symmetry that this group element belongs to.")
    t = property(operator.itemgetter(1),
                 doc="The translational part of the group element.")
    r = property(operator.itemgetter(2),
                 doc="The point group part of the group element.")

    def __new__(cls, sym, t, r, _i_know_what_i_do=False):
        if _i_know_what_i_do:
            return tuple.__new__(cls, (sym, t, r))
        t, r = sym.normalize_element(t, r)
        return tuple.__new__(cls, (sym, t, r))

    # All methods are just proxies for the associated methods of the symmetry.
    def __eq__(self, other):
        return self.sym.eq(self, other)

    def __lt__(self, other):
        return self.sym.lt(self, other)

    def __hash__(self):
        return self.sym.hash(self)

    def __mul__(self, other):
        return self.sym.mul(self, other)

    def __rmul__(self, other):
        return self.sym.mul(other, self)

    def inv(self):
        return self.sym.inv(self)

    def __pow__(self, p):
        if p != int(p):
            raise ValueError(
                "group elements can only be raised to integer powers.")
        if p == 0:
            return self.sym.id

        el = self
        if p < 0:
            el = self.sym.inv(el)
            p = -p

        return reduce(self.sym.mul, [el] * p)

    def __repr__(self):
        return 'GroupElement({}, {}, {})'.format(*map(repr, self))


class Symmetry(metaclass=abc.ABCMeta):
    """Abstract low-level symmetries.

    Attributes
    ----------
    translations : real matrix, or None
        A real matrix where the rows are the realspace lattice vectors of
        the symmetry. If 'None' then the symmetry has no translational part.
    quotient_group: sequence of GroupElements
        Representative elements from the quotient group G/T
        of the left cosets of T in G, excluding the identity.
    """

    @abc.abstractmethod
    def normalize_element(self, t, r):
        """Return a normalized version of a group element.

        This method should check that the combination of 't' and 'r' is
        compatible with the symmetry.
        """

    @abc.abstractmethod
    def mul(self, g1, g2):
        """Act with the group operation between two group elements."""

    @abc.abstractmethod
    def inv(self, g):
        """Return the inverse of the element g."""

    @abc.abstractmethod
    def eq(self, g1, g2):
        """Return True if 'g1' and 'g2' are equal."""

    @abc.abstractmethod
    def lt(self, g1, g2):
        """Return True if 'g1' is less than 'g2'."""

    @abc.abstractmethod
    def hash(self, g):
        """Hash a group element."""

    @property
    def id(self):
        """Return the identity element."""
        return GroupElement(self, None, None)


class NoSymmetry(Symmetry):
    """The trivial symmetry group."""

    def normalize_element(self, t, r):
        if t is not None or r is not None:
            raise ValueError('Group element must be trivial')
        return (None, None)

    def eq(self, g1, g2):
        if g1.sym != self or g2.sym != self:
            raise TypeError()
        assert g1.t is None and g1.r is None
        assert g2.t is None and g2.r is None
        return True

    def mul(self, g1, g2):
        if g1.sym != self or g2.sym != self:
            raise TypeError()
        assert g1 == self.id and g2 == self.id
        return self.id

    def hash(self, g):
        if g.sym != self:
            raise TypeError()
        return hash(g)

    def inv(self, g):
        if g.sym != self:
            raise TypeError()
        assert g == self.id
        return self.id


class System(metaclass=abc.ABCMeta):
    """Abstract general low-level system.

    Attributes
    ----------
    num_nodes: int
        The number of sites in the (fundamental domain
        of the stored_subgroup of) the system.
    num_entries: int
        The number of entries (onsite elements + hopping elements) in the
        system.
    node_ranges : sorted sequence of triples of integers
        Encodes ranges of sites that transform in the same way under the system's
        symmetry. This implies that they must have the same number of orbitals.
        Each triple consists of ``(first_node, norbs, orb_offset)``:
        the first node in the range, the number of orbitals on each node in the
        range, and the offset of the first orbital of the first node in the
        range.  In addition, the final triple should have the form
        ``(num_nodes, 0, tot_norbs)`` where ``tot_norbs`` is the
        total number of orbitals in the system.
    entry_ranges: sorted sequence of tuples
        Each element is ``(start_entry, to_node_range, from_node_range, g)``:
        the first entry in the range, the node ranges to/from which entries
        in this range hop, and the symmetry element to apply to the
        "to" node to get the true hopping. The final tuple should have the
        form ``(num_entries, -1, -1, None)``.
    symmetry: Symmetry
        The full symmetry group of the system.

    Notes
    -----
    Entries are ordered hoppings first and onsites afterwards, so the
    number of hoppings in the system is ``num_entries - num_nodes``.

    Nodes in the same node range have the same site point group *and also*
    the same Hilbert space action.
    """

    @abc.abstractmethod
    def entry_values(self, entries, **kwargs):
        """Get submatrices (entries) of the full Hamiltonian.

        Each Hamiltonian entry corresponds to an onsite or hopping element.

        Parameters
        ----------
        entries: sequence of int, or slice
            If a sequence of integers or a slice is provided, these must
            correspond to entries from the same entry range.
        kwargs: dict
            Arguments to any Hamiltonian value functions.

        Returns
        -------
        Sequence of 2D complex arrays

        Raises
        ------
        IndexError
            If any element of entries >= num_entries.
        """

    @abc.abstractmethod
    def node_pairs(self, entries):
        """Get tail and head nodes of entries.

        Parameters
        ----------
        entries: sequence of int, or slice
            If a sequence of integers is provided, these must
            correspond to entries from the same entry range.

        Returns
        -------
        Pair of integer sequences
            The (heads, tails) nodes of the entries.

        Raises
        ------
        IndexError
            If any element of entries >= num_entries.
        """

    @abc.abstractmethod
    def edges(self, heads, tails):
        """Get the edges corresponding to some hoppings.

        Parameters
        ----------
        heads, tails: sequence of int
            The heads and tails of the hoppings to get.

        Returns
        -------
        Sequence of int
            The edges corresponding to the provided hoppings

        Raises
        ------
        IndexError
            If any element of heads, tails >= num_nodes
        KeyError
            If a given hopping does not exist as an edge in the system
        """

    @abc.abstractmethod
    def neighbors(self, nodes):
        """Get the neighbors of nodes and the corresponding edges.

        Parameters
        ----------
        nodes: sequence of int, or slice

        Returns
        -------
        (neighbor_ranges, neighbor_nodes, entries)
            neighbor_ranges: a sequence of indices into neighbor_nodes
                and entries, which label the start of the neighbors.
                Has len(nodes).
            neighbor_nodes: a sequence of nodes
            entries: a sequence of edges

        Raises
        ------
        IndexError
            If any element of nodes >= num_nodes.
        """

    @abc.abstractmethod
    def sites(self, nodes):
        """Get high-level sites from nodes.

        Parameters
        ----------
        nodes: sequence of int, or slice

        Returns
        -------
        SiteArray

        Raises
        ------
        IndexError
            If any element of nodes >= num_nodes.
        """

    @abc.abstractmethod
    def nodes(self, sites):
        """Get nodes associated with high-level sites.

        Parameters
        ----------
        sites: SiteArray

        Returns
        -------
        Sequence of int

        Raises
        ------
        KeyError
            if a site does not exist in the system.
        """

    ### Symmetry parts of API

    @abc.abstractmethod
    def node_images(self, g, nodes):
        """Get the images of some nodes under a symmetry operation.

        Parameters
        ----------
        g: GroupElement
        nodes: sequence of int

        Returns
        -------
        (image_nodes, g_mapped)
            image_nodes are the images of the nodes under the action of g,
            or -1 if g maps a given node outside the domain stored in the
            system.
            g_mapped is a sequence of group elements from the abelian normal
            subgroup that are in stored_subgroup. Non-trivial elements mean
            that image_node actually corresponds to the node number in
            the domain indexed by g_mapped.

        Raises
        ------
        IndexError
            If any element of nodes >= num_nodes.

        Notes
        -----
        If node i maps to i and its g_mapped is the trivial element, this means
        that i is invariant under the action of g (this places a constraint
        on the onsite Hamiltonian of i).
        -1 is typically returned for a node when g is in stored_subgroup,
        except if the node lies at the boundary of the domain.
        """

    @abc.abstractmethod
    def expand(self, symmetry_subgroup):
        """Create a system with lower symmetry but a larger fundamental domain.

        Parameters
        ----------
        symmetry_subgroup: sequence of GroupElements
            Elements of the subgroup to expand into.
            For elements in the abelian normal subgroup, only
            the generators are provided.

        Returns
        -------
        System
        """

    @abc.abstractmethod
    def hilbert_space_action(self, g, node_range):
        """Get the action of a symmetry element on the site Hilbert space.

        The Hilbert spaces of all the nodes in a given node range transform in
        the same way under the symmetry.

        Parameters
        ----------
        g: GroupElement
        node_range: int
            The index of a node range.

        Returns
        -------
        A LinearOperator.

        Raises
        ------
        IndexError
            If 'node_range >= len(node_ranges)'.
        KeyError
            If g is not a group element.
        """


# High-level site tools

Site = kwant.builder.Site


def trivial_representation(norbs):
    return lambda _: ta.identity(norbs)


@total_ordering
class SiteFamily(kwant.builder.SiteFamily):
    """A family of sites.

    Attributes
    ----------
    canonical_repr : orderable, hashable python object
        Used for comparing site families.
    hash : int
        The has of 'canonical_repr'.
    name : str, or None
        The name of the site family.
    unitary_representation : callable
        Maps GroupElements to unitary matrices.
    """

    def __init__(self, canonical_repr, name, unitary_repr):
        self.canonical_repr = canonical_repr
        self.hash = hash(canonical_repr)
        self.name = name

        if unitary_repr is None:
            unitary_repr = trivial_representation(1)
        elif isinstance(unitary_repr, int):
            if unitary_repr <= 0:
                raise ValueError('"unitary_repr" must be a function or a '
                                 'positive integer')
            unitary_repr = trivial_representation(unitary_repr)
        elif not callable(unitary_repr):
            raise TypeError('"unitary_repr" must be a function or a '
                            'positive integer')
        self.unitary_representation = unitary_repr

    def __str__(self):
        if self.name:
            msg = '<{0} site family {1}{2}>'
        else:
            msg = '<unnamed {0} site family{2}>'
        rep = (' with unitary representation {0}'
               .format(self.unitary_representation))
        return msg.format(self.__class__.__name__, self.name, rep)
