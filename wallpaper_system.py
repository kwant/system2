# -*- coding: utf-8 -*-
# Copyright 2011-2017 Kwant authors.
#
# This file is part of Kwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of Kwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.
"""Low-level system implementation that has wallpaper group symmetry.
"""

from collections import OrderedDict
import functools as ft
import itertools as it
import operator
import bisect
import cmath
from math import pi

import numpy as np
import scipy.sparse as sp
import tinyarray as ta

import system
import wallpaper_group as wg


# Hermitian conjugation that also works for a sequence of matrices
def _herm_conj(v):
    if len(v.shape) == 2:
        return v.conjugate().transpose()
    elif len(v.shape) == 3:  # A sequence of matrices
        assert v.shape[1] == v.shape[2]
        return v.conjugate().transpose(0, 2, 1)


def _are_hermitian(values):
    v = np.asarray(values)
    return np.allclose(v, _herm_conj(v))


def _make_onsite_entries(sym, node_ranges, node_range_cosets,
                         onsite_values):
    nnodes, norbs = zip(*node_ranges)
    start_nodes = [0] + list(it.accumulate(nnodes))
    assert sum(nnodes) == len(onsite_values)
    assert len(node_ranges) == len(node_range_cosets)

    # TODO: have this also group by value for vectorized evaluation
    # Group onsites into ranges.
    def onsite_key(node):
        node_range = bisect.bisect(start_nodes, node) - 1
        return node_range

    # This loop could have been written in a more efficient manner,
    # using the fact that 'onsite_values' are already indexed by
    # node, and that nodes are already grouped into ranges. We write
    # this in a less efficient manner so that the similarities with
    # '_make_hopping_entries' are more apparent, and it will be easier
    # to make several entry ranges for a single node range when we want
    # to group by value (for vectorized evaluation).
    node_pairs = []
    values = []
    entry_ranges = []
    nodes = range(len(onsite_values))
    grouped_onsites = it.groupby(nodes, onsite_key)
    for node_range, nodes in grouped_onsites:
        nodes = list(nodes)
        assert len(nodes) == nnodes[node_range]

        # offsets must have shape (2, n)
        node_pairs.append(np.vstack([nodes, nodes]))

        values.append([onsite_values[node] for node in nodes])

        # num_entries, to_range, from_range, group_element
        entry_ranges.append((len(nodes), node_range, node_range, sym.id))

    return node_pairs, values, entry_ranges


def _make_hopping_entries(node_ranges, node_range_cosets,
                          hopping_values, is_hermitian):
    nnodes, norbs = zip(*node_ranges)
    start_nodes = [0] + list(it.accumulate(nnodes))
    assert len(node_ranges) == len(node_range_cosets)

    def to_range(node):
        return bisect.bisect(start_nodes, node) - 1

    # TODO: have this also group by value for vectorized evaluation
    # Group hoppings into ranges.
    def hop_key(hop):
        (node_a, node_b, g) = hop
        range_a, range_b = to_range(node_a), to_range(node_b)
        sites_equal = node_a == node_b
        # In principle we should group together g's in the same coset of
        # 'range_b', but this is extra work, and splitting entries into
        # different ranges is never wrong, just potentially less efficient.
        return (g, sites_equal, range_a, range_b)

    node_pairs = []
    values = []
    entry_ranges = []
    grouped_hoppings = it.groupby(sorted(hopping_values, key=hop_key), hop_key)
    for (g, sites_equal, range_a, range_b), hops in grouped_hoppings:
        hops = list(hops)
        a_nodes, b_nodes, gs = zip(*hops)
        # Sanity checks
        assert all(g == gg for gg in gs)
        assert all(0 <= node - start_nodes[range_a] < nnodes[range_a]
                   for node in a_nodes)
        assert all(0 <= node - start_nodes[range_b] < nnodes[range_b]
                   for node in b_nodes)

        node_pairs.append(np.vstack([a_nodes, b_nodes]))
        values.append([hopping_values[hop] for hop in hops])
        # num_entries, to_range, from_range, group_element
        entry_ranges.append((len(hops), range_a, range_b, g))

    return node_pairs, values, entry_ranges


# XXX: Inherit from ABC System. We removed the inheritance because we are lazy
#      to implement all the abtract methods.
class System:
    """A system with a wallpaper symmetry group.

    Parameters
    ----------
    sym : wallpaper_group.Symmetry
        The symmetry group of the system
    fundamental_domain : dict : frozenset of GroupElement -> set of Site
        The sites of the fundamental domain, grouped by their point group
        (wyckoff position).
    onsite_values : dict : (Site) -> value
    hopping_values : dict : (Site, Site, GroupElement) -> value
        The keys have the format:
            a, b : Site
                The 'to' and 'from' sites of the hopping, both in the
                'fundamental_sites'
            g : GroupElement
                An element of 'sym' to apply to 'from_site'.
        The tuple `(a, b, g)` represents the hopping `(a, g.b)` where `.` is
        group action on sites.
    unitary_rep : callable: GroupElement -> unitary matrix
        The unitary representation of onsite Hilbert spaces. Also has an
        attribute, 'norbs', that gives the dimension of the representation.
    is_hermitian : bool, default: True

    Attributes
    ----------
    symmetry: Symmetry
        The full symmetry group of the system.
    node_ranges : sequence of tuples
        Encodes ranges of sites that transform in the same way under the
        system's symmetry. This implies that they must have the same number of
        orbitals.  Each tuple consists of:
            nnodes : int
                The number of nodes in this range
            norbs : int
                The number of orbitals for nodes (sites) in this range
    node_range_cosets : sequence of sequences of sets of GroupElements
        The cosets for each node range. By convention the first coset
        is also the symmetry group for the nodes in that range. The coset
        ordering uniquely defines the node ordering in the translational
        unit cell. If we have 3 node ranges 'a', 'b', and 'c', with 'a' having
        4 cosets, 'b' having 2 and 'c' having 3, then the ordering in the
        unit cell is:

            a|b|c|a'|a''|a'''|b'|c'|c''

        where the number of primes denotes the index of the coset of the
        appropriate node range.
    entry_ranges: sorted sequence of tuples
        Encodes ranges of entries (i.e. Hamiltonian blocks between nodes)
        that go to/from nodes in the same range, and that transform in the
            nentries : int
                The number of entries in the range.
            to_node_range : int
                The node range to which entries in this entry range map.
            from_node_range : int
                node range, of which this entry range is related by symmetry.
            g : GroupElement
                A transformation that takes node from `from_node_range` to
                the appropriate image. May contain translational and point
                group parts.
        The symmetry group of the entries in each of the ranges.


    Private Attributes
    ------------------
    _sites : sequence of integer tuples
        The sites in the fundamental domain.
    _node_by_site : dict : integer tuple -> integer
        Reverse of the '_sites' mapping.
    _entry_values : nested sequences of values
        A sequence of values per entry range.
    _entry_node_pairs : sequence of 2xn integer arrays
        One sequence per entry range. The to/from nodes for each entry.
    _unitary_rep : callable : GroupElement, site -> unitary matrix
    """

    def __init__(self, sym, fundamental_domain,
                 onsite_values, hopping_values, unitary_rep,
                 is_hermitian=True):
        # Sanity checks.
        fd_sites = set(it.chain(*fundamental_domain.values()))
        if any(site not in fd_sites for site in onsite_values.keys()):
            raise ValueError('Value given to site outside of '
                             'the fundamental domain')
        hop_sites = [(a, b) for a, b, _ in hopping_values.keys()]
        hop_sites = set(it.chain(*hop_sites))
        if any(site not in fd_sites for site in hop_sites):
            raise ValueError('Hopping provided with sites outside the '
                             'fundamental domain')

        ## Define nodes by ordering sites and group into node ranges.
        # TODO: Allow different unitary representation per site
        nodes_per_range = [len(sites) for sites in fundamental_domain.values()]
        norbs = unitary_rep(sym.id).shape[0]
        orbs_per_node = it.repeat(norbs)  # Here we assume sites have 1 orbital
        node_ranges = list(zip(nodes_per_range, orbs_per_node))
        _sites = ft.reduce(operator.add,
                           map(sorted, fundamental_domain.values()))
        _node_by_site = {site: i for i, site in enumerate(_sites)}
        # Order the cosets of each node range. This defines the ordering for
        # image node ranges in the unit cell. The first coset is also the site
        # group itself.
        node_range_cosets = [wg.cosets(sym, site_group)
                             for site_group in fundamental_domain.keys()]

        # Convert site objects to nodes.
        onsite_values = [onsite_values[s] for s in _sites]
        hopping_values = {(_node_by_site[a], _node_by_site[b], g): v
                          for (a, b, g), v in hopping_values.items()}

        ## Generate entries and fundamental entry ranges.

        _onsite_node_pairs, _onsite_entry_values, _onsite_entry_ranges =\
            _make_onsite_entries(sym, node_ranges, node_range_cosets,
                                 onsite_values)

        _hopping_node_pairs, _hopping_entry_values, _hopping_entry_ranges =\
            _make_hopping_entries(node_ranges, node_range_cosets,
                                  hopping_values, is_hermitian=is_hermitian)

        self.symmetry = sym
        # Node-related attributes
        self.node_ranges = node_ranges
        self.node_range_cosets = node_range_cosets
        self._sites = _sites
        self._node_by_site = _node_by_site
        # Entry-related attributes
        self.entry_ranges = _onsite_entry_ranges + _hopping_entry_ranges
        self._num_onsite_ranges = len(_onsite_entry_ranges)
        self._entry_values = _onsite_entry_values + _hopping_entry_values
        self._entry_node_pairs = _onsite_node_pairs + _hopping_node_pairs
        self._is_hermitian = is_hermitian
        self._unitary_rep = unitary_rep

    def hamiltonian(self, *args):
        node_ranges = self.node_ranges
        node_range_cosets = self.node_range_cosets
        node_offsets = [0, *it.accumulate(nnodes for nnodes, _ in node_ranges)]

        # Calculate the orbital offsets of all node ranges and their images.
        # We store this information as a list of lists.
        # XXX: This could be done on init.
        fd_offset = 0
        image_offset = sum(nnodes * norbs for nnodes, norbs in node_ranges)
        orb_offsets = []
        for range_idx, (nnodes, norbs) in enumerate(node_ranges):
            # Minus 1 because we already have the original
            n_images = len(node_range_cosets[range_idx]) - 1
            range_size = nnodes * norbs
            orb_offsets.append([fd_offset, *(image_offset + range_size * i
                                             for i in range(n_images))])
            fd_offset += range_size
            image_offset += range_size * n_images
        # Total number of orbs in UC
        tot_norbs = image_offset

        # Make an array of orbital offsets from: a node range, and a
        # sequence of nodes in that range. Just add the appropriate
        # 'orb_offset' to get actual orbitals.
        def make_orb_offs(range_index, nodes):
            nnodes, norbs = node_ranges[range_index]
            offsets = np.asarray(nodes) - node_offsets[range_index]
            assert np.all(0 <= offsets) and np.all(offsets < nnodes)

            orb_offs = norbs * offsets.reshape(-1, 1)
            each = np.arange(norbs)
            return (orb_offs + each).reshape(-1)

        # We need to properly repeat column/row indices when building COO
        # format matrices from the (small) dense Hamiltonian values.

        # Column indices go like (1, 2, 3, 1, 2, 3, …)
        def make_from_orbs(orbs, n_to_orbs):
            return np.tile(orbs, n_to_orbs)

        # Row indices go like (1, 1, 2, 2, 3, 3, …)
        def make_to_orbs(orbs, n_from_orbs):
            return np.repeat(orbs, n_from_orbs)

        # Act with 'g' on the Hilbert spaces of sites in 'to_range'
        # and 'from_range'.
        def apply_unitary(g, H, to_range, from_range=None):
            U_to_g = self.hilbert_space_action(g, to_range)
            if from_range is None:
                U_from_g = U_to_g
            else:
                U_from_g = self.hilbert_space_action(g, from_range)
            # This will work for 3D H because of numpy broadcasting.
            return U_to_g.conjugate().transpose() @ H @ U_from_g

        # Generate the Hamiltonian.
        # We will generate a sequence of tuples: (t, rows, cols, data),
        # where 't' is a translation in the basis of the symmetry
        # translation vectors, 'rows' and 'cols'
        # are 1D integer arrays, and 'data' is a 1D complex array.
        # This encodes H(k). In principle we could also pre-sum all
        # entry ranges with the same 't'.
        H_k = []

        onsite_ranges = enumerate(self.entry_ranges[:self._num_onsite_ranges])
        for entry_range_index, entry_range in onsite_ranges:
            nentries, to_range, from_range, g = entry_range
            assert to_range == from_range
            assert g == self.symmetry.id

            # Calculate orbital offsets. XXX: This could be done on init.
            _, norbs = self.node_ranges[to_range]
            to_nodes, _ = self._entry_node_pairs[entry_range_index]
            orb_offs = make_orb_offs(to_range, to_nodes)
            to_orb_offs = make_to_orbs(orb_offs, norbs)
            from_orb_offs = make_from_orbs(orb_offs, norbs)

            cosets = node_range_cosets[to_range]

            # TODO: replace with vectorized function evaluation
            # Get values and verify they satisfy symmetry constraints.
            values = np.asarray(self._entry_values[entry_range_index])
            assert values.shape == (nentries, norbs, norbs)
            if self._is_hermitian and not _are_hermitian(values):
                raise ValueError('Hamiltonian is not Hermitian')
            for c in cosets[0]:  # First coset is symmetry group.
                # H_aa == [U_a(g)]^dagger H_aa U_a(g)
                if not np.allclose(apply_unitary(c, values, to_range), values):
                    raise ValueError('Hamiltonian does not respect '
                                     'the declared symmetry.')

            # Generate all image onsites. Note that this includes the FD,
            # although in principle we can pull it out of the loop to avoid
            # the dense matrix multiplication.
            for coset_index, coset in enumerate(cosets):
                # Generate orbitals by adding the offset for this node range
                range_offset = orb_offsets[to_range][coset_index]
                to_orbs = range_offset + to_orb_offs
                from_orbs = range_offset + from_orb_offs
                # Transform the values
                c = next(iter(coset))  # Choose arbitrary coset element.
                H_prime = apply_unitary(c, values, to_range)

                H_k.append((self.symmetry.id.t, to_orbs, from_orbs,
                            H_prime.reshape(-1)))

        hopping_ranges = enumerate(self.entry_ranges[self._num_onsite_ranges:],
                                   self._num_onsite_ranges)
        for entry_range_index, entry_range in hopping_ranges:
            nentries, to_range, from_range, g = entry_range

            # Calculate orbital offsets. XXX: This could be done on init.
            _, to_norbs = self.node_ranges[to_range]
            _, from_norbs = self.node_ranges[from_range]
            to_nodes, from_nodes = self._entry_node_pairs[entry_range_index]
            to_orb_offs = make_orb_offs(to_range, to_nodes)
            to_orb_offs = make_to_orbs(to_orb_offs, from_norbs)
            from_orb_offs = make_orb_offs(from_range, from_nodes)
            from_orb_offs = make_from_orbs(from_orb_offs, to_norbs)

            # Generate hopping symmetry group. XXX: This could be done on init.
            to_cosets = node_range_cosets[to_range]
            from_cosets = node_range_cosets[from_range]
            hop_invariant_group, hop_exchange_set = wg.hopping_group(
                to_cosets[0], from_cosets[0], g)
            # If the Hamiltonian is Hermitian then reversed hoppings should be
            # generated by Hermitian conjugation; we include group elements
            # that exchange the hopping's ends in the symmetry group to avoid
            # generating these reversed hoppings when creating the images.  If
            # it is *not* Hermitian, then reversed hoppings need to be
            # generated by applying group elements, so we exclude these
            # elements from the hopping symmetry group.
            if np.any(to_nodes != from_nodes) or not self._is_hermitian:
                hop_group = hop_invariant_group
                hop_exchange_set = set()
            else:
                hop_group = hop_invariant_group | hop_exchange_set

            hop_cosets = wg.cosets(self.symmetry, hop_group)

            # TODO: replace with vectorized function evaluation
            # Get values and verify they satisfy symmetry constraints.
            values = np.asarray(self._entry_values[entry_range_index])
            assert values.shape == (nentries, to_norbs, from_norbs)
            for c in hop_group:
                H_prime = apply_unitary(c, values, to_range, from_range)
                if c in hop_exchange_set:
                    H_prime = _herm_conj(H_prime)
                # H_ab == [U_a(g)]^dagger H_ab U_a(g) if not exchanging ends,
                # else [H_ab]^dag == [U_a(g)]^dagger H_ab U_a(g)
                if not np.allclose(H_prime, values):
                    raise ValueError('Hamiltonian does not respect '
                                     'the declared symmetry.')

            # Generate all image hoppings. Note that this includes the
            # fundamental hoppings.
            for coset_idx, hop_coset, in enumerate(hop_cosets):
                c = next(iter(hop_coset))  # Choose arbitrary coset element
                # Calculate which coset 'c' is a part of for the to/from sites;
                # this gives us the image in the unit cell.
                to_coset_index, to_c = wg.which_coset(to_cosets, c)
                from_coset_index, from_c = wg.which_coset(from_cosets, c * g)
                # Find the remaining translational part of the hopping, after
                # having transformed by 'c'.
                t = to_c * g * from_c.inv()

                # Generate orbitals by adding the offsets for this coset image.
                to_range_offset = orb_offsets[to_range][to_coset_index]
                to_orbs = to_range_offset + to_orb_offs
                from_range_offset = orb_offsets[from_range][from_coset_index]
                from_orbs = from_range_offset + from_orb_offs

                # Transform the values
                H_prime = apply_unitary(c, values, to_range, from_range)

                H_k.append((t.t, to_orbs, from_orbs, H_prime.reshape(-1)))

                # Add Hermitian conjugate entry, with the inverse translation
                # element. Note that this operation could be pulled out of this
                # loop and run as a step in 'h_k', below, or moved somewhere
                # else entirely when we implement operators as sums of others,
                # and would write H(k) = H_0 + V + V_dagger
                if self._is_hermitian:
                    H_k.append((t.inv().t, from_orbs, to_orbs,
                                _herm_conj(H_prime).reshape(-1)))

        return _Hamiltonian(H_k, tot_norbs)

    def hilbert_space_action(self, g, node_range):
        # For now all node ranges have the same representation
        return self._unitary_rep(g)


class _Hamiltonian:

    def __init__(self, h_parts, tot_norbs):
        self.h_parts = h_parts
        self.tot_norbs = tot_norbs

    def __call__(self, k):
        rows = []
        cols = []
        data = []
        for t, r, c, h in self.h_parts:
            # k is in units of reciprocal lattice vectors,
            # which takes care of the factor of 2π.
            exp_ikt = cmath.exp(2 * 1j * pi * ta.dot(k, t))
            rows.append(r)
            cols.append(c)
            data.append(exp_ikt * h)

        rows, cols, data = map(np.hstack, (rows, cols, data))
        h_k = sp.coo_matrix((data, (rows, cols)),
                            shape=(self.tot_norbs, self.tot_norbs))
        h_k.sum_duplicates()  # ensure H(k) is in canonical COO format
        return h_k


def spectrum(syst, grid=20):
    """Calculate the spectrum of 'syst' over the Brillouin zone.

    Parameters
    ----------
    syst : System
    grid : int or Lattice

    Returns
    -------
    plotly plot
    """
    if isinstance(grid, int):
        klat = wg.Lattice(syst.symmetry.reciprocal(), div=grid)
    else:
        # Should check that the grid symmetry matches the reciprocal
        # of syst.symmetry, but we are lazy.
        klat = grid

    irreducible_wedge, _ = wg.fundamental_domain(klat)
    H = syst.hamiltonian()

    kpoints = []
    Ek = []
    for G, kspace_sites in irreducible_wedge.items():
        k_tags = [site.tag for site in kspace_sites]
        # Divide by the grid to get the tag in units of the original
        # sym.reciprocal() translation vectors.
        eigenvals = [np.linalg.eigvalsh(H(k / klat.div).todense())
                     for k in k_tags]
        # Generate all images.
        for c in wg.cosets(klat.sym, G):
            g = next(iter(c))  # arbitrary coset representative
            image_tags = [klat.act(g, tag) for tag in k_tags]
            kpoints.extend(klat.pos(tag) for tag in image_tags)
            Ek.extend(eigenvals)

    Ek = np.array(Ek).transpose()
    kpoints = np.array(kpoints)
    kx, ky = kpoints.transpose()

    # Plot the spectrum
    from scipy.spatial import Delaunay
    import plotly.figure_factory as FF

    simplices = Delaunay(kpoints).simplices

    # Treat different bands as a single "surface"
    # for the purposes of plotting, as plotly cannot
    # plot multiple surfaces.
    kx = np.tile(kx, len(Ek))
    ky = np.tile(ky, len(Ek))
    simplices = np.vstack(simplices + i * Ek.shape[1] for i in range(len(Ek)))
    Ek = Ek.reshape(-1)

    return FF.create_trisurf(kx, ky, Ek, simplices)


def test_system():
    raise NotImplementedError()


if __name__ == '__main__':
    test_system()
