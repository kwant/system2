# -*- coding: utf-8 -*-
# Copyright 2011-2017 Kwant authors.
#
# This file is part of Kwant.  It is subject to the license terms in the file
# LICENSE.rst found in the top-level directory of this distribution and at
# http://kwant-project.org/license.  A list of Kwant authors can be found in
# the file AUTHORS.rst at the top-level directory of this distribution and at
# http://kwant-project.org/authors.
"""Concrete implementation of 2D wallpaper group and associated systems.

In this module we define 'sites' as being uniquely identified by a vector
of integers, which are the site's coordinates with respect to some lattice.
The particular lattice in question will depend on the context in which the
site is used (typically the context will be a particular symmetry group).

In this module we define 'hoppings' as pairs of sites. There are 2 standard
ways in which hoppings may be represented:

1. The "natural" representation, '(i, j)', where 'i' and 'j' are sites
   integer vectors),
2. The "reduced" representation, '(i, j, g)', where 'i' and 'j' are sites
   in the fundamental domain of some symmetry, and 'g' is a group
   element of that same symmetry. There is a natural mapping from the
   reduced to the natural representation: '(i, j, g) -> (i, g @ j)',
   where '@' is taken to mean the group action on sites.
"""

import itertools as it
import functools as ft
import operator
from collections import defaultdict

import numpy as np
import numpy.linalg as la
import scipy as sp
import tinyarray as ta

import system
from system import GroupElement, Site


# Invert a 2x2 matrix
def _inv_2by2(A):
    assert A.shape == (2, 2)
    a, b, c, d = A[0, 0], A[0, 1], A[1, 0], A[1, 1]
    det = a * d - b * c
    return ta.array([[d, -b], [-c, a]]) / det


# Invert an integer matrix
def _inv_int(A):
    _A = ta.array(A, int)
    if _A != A or abs(la.det(A)) != 1:
        raise ValueError('Input needs to be an invertible integer matrix')
    return ta.array(la.inv(_A), int)


# Find the determinant of an integer matrix
def _det_int(A):
    return int(la.det(ta.array(A, int)))


def _take_first_seen(iterator, *, key=lambda v: v):
    seen = set()
    for v in iterator:
        kv = key(v)
        if kv not in seen:
            seen.add(kv)
            yield v


def cvp(vec, basis, n=1, rtol=1e-09):
    """
    Solve the closest vector problem for a vector, given a basis.

    This algorithm performs poorly in general, so it should be supplied
    with LLL-reduced basis or Voronoi vectors.

    Parameters
    ----------
    vec : 1d array-like of floats
        The lattice vectors closest to this vector are to be found.
    basis : 2d array-like of floats
        Sequence of basis vectors
    n : int
        Number of lattice vectors closest to the point that need to be found.
        If 'n = -1', all the closest vectors that are equidistant within 'rtol'
        are found.
    rtol: If 'n = -1', tolerance when deciding whether points are equidistant.

    Returns
    -------
    coords : numpy array
        An array with the coefficients of the 'n' closest lattice vectors to
        the requested point.

    Notes
    -----
    This function can also be used to solve the `n` shortest lattice vector
    problem if the `vec` is zero, and `n+1` points are requested
    (and the first output is ignored).
    """
    # Calculate coordinates of the starting point in this basis.
    basis = np.asarray(basis)
    if basis.ndim != 2:
        raise ValueError('`basis` must be a 2d array-like object.')
    # Project the coordinates on the space spanned by 'basis', in
    # units of 'basis' vectors.
    vec_bas = sp.linalg.lstsq(basis.T, vec)[0]
    # Round the coordinates, this finds the lattice point which is
    # the center of the parallelepiped that contains 'vec'.
    center_coords = np.array(np.round(vec_bas), int)
    # Reconstruct the projection, needed if 'basis' does not span
    # all the dimensions in space.
    vec = vec_bas @ basis
    # The volume of a parallelepiped spanned by 'basis' is 'sqrt(det(bbt))'
    # Calculate the 'area' of each 'face' spanned by one less vector. The
    # perpendicular size is volume/area. Utilizie the relation
    # between cofactors and inverse matrix. Choose the smallest perpendicular
    # size as the diameter of the largest sphere inside.
    bbt = basis @ basis.T
    rad = 0.5 / np.sqrt(np.max(np.diag(sp.linalg.inv(bbt))))

    l = 1
    while True:
        # Make all the lattice points in and on the edges of a parallelepiped
        # of 2*l size around center_coords.
        points = np.mgrid[tuple(slice(i - l, i + l + 1)
                          for i in center_coords)]
        points = points.reshape(basis.shape[0], -1).T
        # If there are less than 'n' points, make more
        if len(points) < n:
            l += 1
            continue
        point_coords = points @ basis
        point_coords = point_coords - vec.T
        distances = sp.linalg.norm(point_coords, axis = 1)
        order = np.argsort(distances)
        distances = distances[order]
        points = points[order]
        if n == -1:
            dist_n = distances[0]
            # Count number of points that are equidistant.
            m = len([1 for d in distances if np.isclose(d, dist_n, rtol=rtol)])
            points_keep = points[:m]
        else:
            rtol = 0
            dist_n = distances[n-1]
            points_keep = points[:n]
        # We covered all points in a sphere of radius (2*l-1) * rad,
        # if the current 'dist_n' is smaller, we surely found all the
        # smaller vectors.
        if dist_n < (2*l - 1 - rtol) * rad:
            return np.array(points_keep, int)
        else:
            # Otherwise there may be smaller vectors we haven't found,
            # increase l.
            l += 1
            continue


class Symmetry(system.Symmetry):
    """A wallpaper group: point group X 2D translations.

    This class represents a wallpaper symmetry group that can act on sites that
    live on a regular lattice compatible with the symmetry, and that are
    uniquely identified by a 2-vector of (integer) lattice coordinates.

    Parameters
    ----------
    translations : 2D real array
        Rows are real space primitive translation vectors.
        XXX For now defaults to 2x2 identity matrix if None
    point_group : sequence
        A specification of the point group of the symmetry. For symmorphic
        groups this is just a sequence of 2x2 integer matrices representing
        rotations in the basis of symmetry vectors. For nonsymmorphic groups
        then the first element is an integer divisor for the translational part
        of the rest of the elements. Each of the elements of the rest is
        either: a 2x2 integer rotation matrix, or a pair '(t, r)' of integer
        translation vector and 2x2 rotation matrix.

    Notes
    -----
    An error will be raised if 'tx' and 'ty' are not divisible by the divisor
    if a nonsymmorphic 'point_group' is specified.

    A full list of wallpaper groups that can be passed as 'point_group'
    are provided as class attributes for convenience:

        >>> p4 = Symmetry(point_group=Symmetry.p4)
        >>> p2 = Symmetry(tx=1, ty=2, point_group=Symmetry.p2)
    """

    p1 = (
        [[1, 0], [0, 1]],  # identity
    )

    p2 = (
        [[1, 0], [0, 1]],  # identity
        [[-1, 0], [0, -1]],  # π rotation
    )

    pm = p1 + (
        [[1, 0], [0, -1]],  # mirror in --
    )

    pg = (
        2,                          # denominator (always 2 in 2D)
        ([0, 0], [[1, 0], [0, 1]]),   # identity
        ([1, 0], [[1, 0], [0, -1]]),  # glide in --
    )

    cm = p1 + (
        [[0, 1], [1, 0]],  # mirror in /
    )

    pmm = p2 + (
        [[1, 0], [0, -1]],  # mirror in --
        [[-1, 0], [0, 1]],  # mirror in |
    )

    pmg = pg + (
        [[0, 0], [[-1, 0], [0, 1]]],  # mirror in |
        [[1, 0], [[-1, 0], [0, -1]]],  # off-center 2-fold rotation
    )

    pgg = pg + (
        [[0, 1], [[-1, 0], [0, 1]]],  # glide in |
        [[1, 1], [[-1, 0], [0, -1]]],  # off-center 2-fold rotation
    )

    cmm = p2 + (
        [[0, 1], [1, 0]],  # mirror in /
        [[0, -1], [-1, 0]],  # mirror in \
    )

    p4 = p2 + (
        [[0, -1], [1, 0]],  # anticlockwise π/2 rotation
        [[0, 1], [-1, 0]],  # clockwise π/2 rotation
    )

    p4m = p4 + (
        [[1, 0], [0, -1]],  # mirror in --
        [[-1, 0], [0, 1]],  # mirror in |
        [[0, 1], [1, 0]],  # mirror in /
        [[0, -1], [-1, 0]],  # mirror in \
    )

    p4g = (
        2,                          # denominator (always 2 in 2D)
        ([0, 0], [[1, 0], [0, 1]]),    # identity
        ([0, 0], [[0, -1], [1, 0]]),   # anticlockwise π/2 rotation
        ([0, 0], [[-1, 0], [0, -1]]),  # π rotation
        ([0, 0], [[0, 1], [-1, 0]]),   # clockwise π/2 rotation
        ([1, 1], [[0, 1], [1, 0]]),    # glide in /
        ([1, 1], [[0, -1], [-1, 0]]),  # glide in \
        ([1, 1], [[-1, 0], [0, 1]]),   # glide in |
        ([1, 1], [[1, 0], [0, -1]]),   # glide in --
    )

    # For hexagonal lattices we use basis vectors [1, 0] and [-0.5, 0.5*3**0.5]
    p3 = (
        [[1, 0], [0, 1]],  # identity
        [[0, -1], [1, -1]],  # anticlockwise 2π/3 rotation
        [[-1, 1], [-1, 0]],  # clockwise 2π/3 rotation
    )

    p3m1 = p3 + (
        [[-1, 1], [0, 1]],   # mirror not containing lattice vector
        [[1, 0], [1, -1]],  # rotated mirror
        [[0, -1], [-1, 0]],  # rotated mirror
    )

    p31m = p3 + (
        [[1, -1], [0, -1]],   # mirror containing lattice vector
        [[-1, 0], [-1, 1]],  # rotated mirror
        [[0, 1], [1, 0]],  # rotated mirror
    )

    p6 = p3 + (
        [[1, -1], [1, 0]],   # anticlockwise π/3 rotation
        [[-1, 0], [0, -1]],  # π rotation
        [[0, 1], [-1, 1]],   # clockwise π/3 rotation
    )

    p6m = p6 + (
        [[-1, 1], [0, 1]],   # mirrors not containing lattice vector
        [[1, 0], [1, -1]],  # rotated mirror
        [[0, -1], [-1, 0]],  # rotated mirror
        [[1, -1], [0, -1]],   # mirrors containing lattice vector
        [[-1, 0], [-1, 1]],  # rotated mirror
        [[0, 1], [1, 0]],  # rotated mirror
    )

    def __init__(self, translations=None, point_group=p1):
        if translations is None:
            # XXX Temporary
            # dim = 0
            translations = ta.identity(2)
            dim = 2
        else:
            dim = len(translations)
            translations = ta.array(translations)
            if (translations.shape != (dim, dim)
                or np.isclose(la.det(translations), 0)):
                raise ValueError('Translations must span space.')

        if dim != 2:
            raise NotImplementedError(
                'Only 2 dimensional lattices are currently supported.')

        # disambiguate symmorphic/non-symmorphic cases
        if isinstance(point_group[0], int):
            d, *point_group = point_group
            if d <= 0 or int(d) != d:
                raise ValueError('Divisor for partial translations must be a '
                                 'positive integer.')
            # New format of point group elements: (tp, R)
            # tp is an integer partial translation in units of
            # lattice vectors / d and R is an invertible integer matrix.
            point_group = [(ta.array(tp, int), ta.array(r, int))
                           for tp, r in point_group]
            point_group = [(ta.array(np.sign(tp) * (tp % d), int), r)
                           for tp, r in point_group]
        else:
            d = 1
            point_group = [(ta.zeros(dim), ta.array(r, int))
                           for r in point_group]
        self.d = d
        self.translations = translations
        self._inv_translations_T = la.inv(translations.transpose())

        # check format of 'point_group'
        if not all(len(g) == 2 for g in point_group):
            raise ValueError("'point_group' has invalid format")
        if len(point_group) != len(set(point_group)):
            raise ValueError("'point_group' contains duplicate elements")
        if not all(tp.shape == (dim,) and r.shape == (dim, dim)
                   for tp, r in point_group):
            raise ValueError("'point_group' has invalid format")

        zeros = ta.zeros(dim)
        point_group = sorted(GroupElement(self, zeros, g) for g in point_group)

        # check that point group forms a group modulo lattice translations.
        prod = {g1 * g2 for g1, g2 in it.product(point_group, point_group)}
        reduced_prod = {g.r for g in prod}
        reduced_point_group = {g.r for g in point_group}
        if not all(g in reduced_point_group for g in reduced_prod):
            raise ValueError("'point_group' does not form a group")

        self.quotient_group = point_group

    def normalize_element(self, t, r):
        _t = ta.array(t, int)
        _tp, _r = (ta.array(v, int) for v in r)
        if _t != t or (_tp, _r) != r:
            raise TypeError('All entries must be integers')

        dim = len(self.translations)
        t_msg = 'must have shape ({},)'.format(dim)
        if _t.shape != (dim,):
            raise ValueError('Translational part ' + t_msg)
        if _tp.shape != (dim,):
            raise ValueError('Partial translation part ' + t_msg)
        if _r.shape != (dim, dim):
            raise ValueError('Rotational part must have shape ({0}, {0})'
                             .format(dim))

        R = ft.reduce(ta.dot, (self.translations.transpose(), _r,
                               self._inv_translations_T))
        id_dim = ta.identity(dim)
        if not np.allclose(ta.dot(R, R.transpose()), id_dim):
            raise ValueError('Point group is incompatible with '
                             'the translation vectors.')

        return _t, (_tp, _r)

    def mul(self, g1, g2):
        d = self.d
        t1, t2 = g1.t, g2.t
        tp1, r1 = g1.r
        tp2, r2 = g2.r
        # Integer part of output translation
        t_out = (d*t1 + tp1 + ta.dot(r1, d*t2 + tp2)) // d
        # Fractional part of output translation
        tp_out = (d*t1 + tp1 + ta.dot(r1, d*t2 + tp2)) % d
        # Output rotation
        r_out = ta.dot(r1, r2)
        return GroupElement(
            self,
            t_out, (tp_out, r_out), _i_know_what_i_do=True)

    def eq(self, g1, g2):
        return g1.t == g2.t and g1.r == g2.r

    def lt(self, g1, g2):
        # Sort group elements, make sure identity is the smallest
        # Otherwise order lexicographicaly
        if (g1 == self.id) ^ (g2 == self.id):
            return g1 == self.id
        else:
            return (ta.abs(g1.t), g1.t, g1.r) < (ta.abs(g2.t), g2.t, g2.r)

    def hash(self, g):
        return hash((g.t, g.r))

    def inv(self, g):
        t = g.t
        tp, r = g.r
        d = self.d
        r_inv = _inv_int(r)  # r is an invertible n x n integer matrix
        t_inv = -ta.dot(r_inv, d*t + tp)
        return GroupElement(self, t_inv//d, (t_inv%d, r_inv),
                            _i_know_what_i_do=True)

    @property
    def id(self):
        dim = len(self.translations)
        return GroupElement(self, ta.zeros(dim, int),
                            (ta.zeros(dim, int), ta.identity(dim, int)),
                            _i_know_what_i_do=True)

    def reciprocal(self):
        """Return the equivalent symmetry in reciprocal space."""
        B = np.array(self.translations.transpose())
        recip_translations = 2 * np.pi * B.dot(np.linalg.inv(B.T.dot(B))).T
        # The reciprocal quotient group is the one where all the partial
        # translations have been set to zero and the rotation matrices are
        # transposed.
        recip_pg = set()
        for g in self.quotient_group:
            tp, r = g.r
            recip_pg.add(r.transpose())
        return self.__class__(recip_translations, list(recip_pg))

    # Methods *not* defined by the abstract base class

    def __contains__(self, item):
        # 'True' if 'self.quotient_group' contains 'item' modulo translations.
        if not isinstance(item, GroupElement):
            raise TypeError('Expected a GroupElement')
        if item.sym != self:
            raise ValueError('GroupElement belongs to the wrong symmetry.')
        # Strip integer translation part and look in 'quotient_group'.
        dim = len(self.translations)
        return (GroupElement(self, ta.zeros(dim, int), item.r)
                in self.quotient_group)

# The following functions implement abstract group-theoretical operations.
# They only require abstract group operations and inversion.


def hopping_group(sg_a, sg_b, g):
    """Find the symmetry group of a hopping '(a, g.b)'.

    Parameters
    ----------
    sg_a, sg_b : sets of GroupElement
        The site groups of sites 'a' and 'b'.
    g : GroupElement
        The group element applied to site 'b' to bring it to
        the true site.

    Returns
    -------
    invariant_group : set of GroupElement
        The group of operations that leave the hopping invariant.
    exchange_set : set of GroupElement
        The set of operations that exchange the ends of the hopping.
        Empty if 'sg_a != sg_b'. The union of this
        set with 'invariant_subgroup' is also a group.
    """
    g_inv = g.inv()
    # Operations that leave both ends invariant
    invariant_group = sg_a & {g * gb * g_inv for gb in sg_b}
    if sg_a != sg_b:
        return invariant_group, set()
    # Operations that exchange ends
    exchange_set = {g * ga for ga in sg_a} & {ga * g_inv for ga in sg_a}
    return invariant_group, exchange_set


# The following functions implement abstract group-theoretical operations.
# They depend on the fact that GroupElements:
#   + Have '<' and '==' defined between them
#   + Have a property 'r', which is the point group
#     part of the group element. This property only needs '==' to be
#     defined.
# If we defined the abstract 'system.GroupElement' class to contain an 'r'
# attribute (which is probably sensible), and define a sorting operation on
# 'system.GroupElement's then these functions could be lifted into 'system.py'.


def cosets(sym, subgroup):
    """Find cosets of the point group of 'sym' with respect to a 'subgroup'.

    This operation is done modulo translations.

    Parameters
    ----------
    sym : Symmetry
    subgroup : list of GroupElement

    Returns
    -------
    sequence of sets of GroupElement
    """
    # Sorting makes sure that identity is a coset representative
    pg = sorted(sym.quotient_group)
    cosets = []
    # Store rotational parts of the cosets, for easy comparison
    cosetsr = set()
    while pg:
        g = pg.pop(0)
        cos = {g * n for n in subgroup}
        cosr = frozenset({c.r for c in cos})
        if cosr not in cosetsr:
            # Add representative and coset
            cosets.append(frozenset(cos))
            # Add rotational part as well
            cosetsr.add(cosr)
            pg = [g for g in pg if g.r not in cosr]
    assert cosets[0] == subgroup
    return cosets


def which_coset(cosets, g):
    for i, coset in enumerate(cosets):
        for c in coset:
            if g.r == c.r:
                return i, c
    else:
        raise RuntimeError('no representative found in cosets')


# Lattices -- SiteFamilies that are aware of spatial symmetries


class Lattice(system.SiteFamily):
    """A family of sites related by a symmetry.

    Sites live on a lattice that is finer than, but compatible with,
    the lattice defined by the translation vectors of 'sym'.

    Parameters
    ----------
    sym : Symmetry
    div : int, default: 1
        The number by which to divide 'sym.translations' by to arrive at
        the underlying lattice.
    lattice : sequence of integer vectors, optional
        Lattice vectors in the basis 'sym.translations / div'.
        If not provided then the identity is used.
    """

    def __init__(self, sym, div=1, lattice=None, name=None, unitary_repr=None):
        if not isinstance(sym, Symmetry):
            raise TypeError("'sym' must be a space group symmetry")
        div = int(div)
        if div <= 0:
            raise ValueError('"div" must be a positive integer')
        if div % sym.d != 0:
            raise ValueError('"div" is incommensurate with the point group.')

        if lattice is None:
            lattice = ta.identity(len(sym.translations), int)
        else:
            lattice = ta.array(lattice, int)

        det_lattice = _det_int(lattice)
        if det_lattice == 0:
            raise ValueError('Lattice must span space.')
        lat_t = lattice.transpose()
        inv_lattice = la.inv(lat_t)

        scaled_inv = inv_lattice * div
        if not np.allclose(scaled_inv, np.round(scaled_inv)):
            raise ValueError('Lattice vectors are incommensurate with the '
                             'symmetry lattice')
        # Check that new lattice vectors are commensurate with the point group.
        for g in sym.quotient_group:
            tp, r = g.r
            # Rotations must still be integer matrices in the new basis.
            rprime = ft.reduce(ta.dot, (det_lattice * inv_lattice, r, lat_t))
            if not np.allclose(rprime % det_lattice, 0):
                raise ValueError("Lattice is incommensurate with "
                                 "rotations in 'sym'")
            # Partial translation must be an integer vector in the new basis.
            tp_prime = ta.dot(inv_lattice, div/sym.d * tp)
            if not np.allclose(tp_prime, ta.round(tp_prime)):
                raise ValueError("Lattice is incommensurate with"
                                 " nonsymmorphic elements in 'sym'")

        args = (sym, div, lattice, name, unitary_repr)
        canonical_repr = '{}({}, {}, {}, {}, {})'.format(self.__class__,
                                                         *map(repr, args))
        super().__init__(canonical_repr, name, unitary_repr)
        self.sym = sym
        self.div = div
        self.translations = lattice

        # We only call CVP on sites in the parallelepipedal unit cell,
        # so we add a cache to speed this up.
        n_uc_sites = div ** len(lattice) // det_lattice
        self.cvp = ft.lru_cache(maxsize=n_uc_sites)(cvp)

    def unit_cell(self):
        """Return all sites in the translational unit cell."""
        dim = len(self.sym.translations)
        # Grid in the basis of 'self.sym.translations / self.div'
        max_tag = (self.div,) * dim
        sym_tags = np.array(list(np.ndindex(*max_tag)))
        # Select tags that are integer combinations of 'self.translations'
        lat_t = self.translations.transpose()
        inv_lat = la.inv(lat_t)
        lat_tags = np.dot(inv_lat, sym_tags.transpose()).transpose()
        lat_tags = [ta.array(ta.round(tag), int) for tag in lat_tags
                    if np.allclose(ta.round(tag), tag)]
        # Map the candidates to the unit cell, removing duplicates
        return set(Site(self, tag)
                   for _, tag in map(self.to_unit_cell, lat_tags))

    # These methods are used by the `Site` class, and work on tags

    def normalize_tag(self, tag):
        tag = ta.array(tag, int)
        if len(tag) != len(self.translations):
            raise ValueError("Dimensionality mismatch.")
        return tag

    def pos(self, tag):
        sym_to_realspace = self.sym.translations.transpose()
        lat_to_sym = self.translations.transpose() / self.div
        return ft.reduce(ta.dot, (sym_to_realspace, lat_to_sym, tag))

    def lt(self, tag1, tag2):
        # If one is closer to the origin,
        # keep that, otherwise the lexicogrphically larger one
        r1 = la.norm(self.pos(tag1))
        r2 = la.norm(self.pos(tag2))
        if np.isclose(r1, r2):
            return tag1 > tag2
        else:
            return r1 < r2

    def act(self, g, tag):
        """Act with a symmetry group element 'g' on 'tag'.

        Parameters
        ----------
        g : GroupElement
            'g.sym' must be 'self.sym'.
        tag : integer vector
        """
        if g.sym != self.sym:
            raise ValueError('Group element must be from the lattice '
                             'symmettry group')
        d = g.sym.d
        tp, r = g.r
        t = g.t
        div = self.div
        lat_t = self.translations.transpose()
        inv_lat = la.inv(lat_t)
        # Transform position to 'self.sym.translations / div' units.
        tag_div = ta.dot(lat_t, tag)
        # Act with the group element
        tag_div = tp * div/d + t * div + ta.dot(r, tag_div)
        # Transform back to original units
        new_tag = ta.dot(inv_lat, tag_div)
        # Round to integers
        new_tag = ta.array(ta.round(new_tag), int)
        return new_tag

    def to_unit_cell(self, tag):
        """Bring site with 'tag' to the unit cell.

        Parameters
        ----------
        tag : integer vector

        Returns
        -------
        g : GroupElement
            The (translation) group element that brings 's_prime' to 'site',
            such that 'tag = act(g, tag_prime)'
        tag_prime : integer vector
            The image of 'site' in the unit cell.

        Notes
        -----
        We return 'g', rather than its inverse, to be consistent with 'to_fd'.
        """
        # This is complicated by the fact that our unit cell
        # is *centred* on (0, 0).
        sym = self.sym
        basis = sym.translations
        div = self.div
        lat_t = self.translations.transpose()
        # Transform position to 'self.sym.translations / div' units.
        tag_div = ta.dot(lat_t, tag)
        # Find translational part in 'self.sym.translations' units
        # and translate back into the parallelepiped UC.
        t1 = (tag_div + (div - 1) // 2) // div
        g1 = GroupElement(sym, t1, sym.id.r)
        tag = self.act(g1.inv(), tag)
        # Real space position
        x = self(*tag).pos
        # Find the closest lattice vectors and select the lexicographically
        # smallest one
        ts = [tuple(t) for t in self.cvp(x, basis, n=-1)]
        t2 = min(ts)
        g2 = GroupElement(sym, t2, sym.id.r)
        return g1 * g2, self.act(g2.inv(), tag)


# The following functions implement operations that use the above
# operations that define group action. In addition they use site
# comparison to decide which one is in the "fundamental domain".


# TODO: Modify this to work with sites from a lattice and a symmetry that
#       is a subgroup of the lattice symmetry.
def fundamental_domain(lat, tags=None):
    """Get the sites in the fundamental domain, and their invariant subgroups.

    The fundamental domain is constructed by starting from the sites provided
    as keys of onsites. Then symmetries in sym are applied to every site sites
    are grouped into cycles under the action of point group elements.  The
    representative from each cycle is chosen by: first choosing the site
    closest to the origin, and then the lexicographically larger site. The
    subgroup of 'sym' that maps the representative to another site in the cycle
    is called the 'invariant subgroup' of the representative.

    Parameters
    ----------
    lat : Lattice
    tags : sequence of tags, optional
        If not provided, then all sites in 'lat.unit_cell()' are used.

    Returns
    -------
    fd : dict: frozenset of GroupElements -> tuple of Sites
        The sites of the fundamental domain, keyed by the subgroup of 'sym'
        that leaves them invariant.
    reverse_map : dict: Site -> GroupElement
        A map from sites in the fundamental domain to the group element
        that takes them to the site originally specified by the user.
        If several original sites map to the same fundamental domain site,
        then the one that appeared first in 'sites' is chosen.
    """
    sym = lat.sym
    if tags is None:
        uc_tags = [(sym.id, site.tag) for site in lat.unit_cell()]
    else:
        # Move all sites to translational unit cell, eliminating duplicates and
        # retaining the group element that brings the UC site to the site
        # specified by the user.
        uc_tags = _take_first_seen(map(lat.to_unit_cell, tags),
                                   key=lambda r: r[1])
    reverse_map = {}
    fd_subgroups = defaultdict(list)

    # Loop over orbits in the translational unit cell, keeping track of
    # the sites we have seen to avoid iterating over the same orbit twice.
    seen = set()
    for t_original, uc_tag in uc_tags:
        if uc_tag in seen:
            continue
        # Representative site for this orbit and group element that takes us
        # from 'uc_site' to the representative site.
        orbit_repr = uc_tag
        orbit_transform = sym.id
        invariant_subgroup = {sym.id}

        for r in lat.sym.quotient_group:
            t, s_prime = lat.to_unit_cell(lat.act(r, uc_tag))
            g = t.inv() * r
            if s_prime == uc_tag:
                # 'g' leaves 's' invariant up to a translation
                invariant_subgroup.add(g)
            else:
                # 'uc_site' and 'sprime' are in the same orbit; keep only the
                # larger site as a representative and the group element by
                # which to later conjugate the site subgroup.
                if lat.lt(s_prime, orbit_repr):
                    orbit_repr = s_prime
                    orbit_transform = g
                seen.add(s_prime)

        # Save the representative site, invariant subgroup, and operation
        # that brings it to the original site specified by the user,

        g, g_inv = orbit_transform, orbit_transform.inv()
        # Conjugate by the group element that takes us from 'uc_site' to the
        # 'orbit_representative'.
        site_group = frozenset({g * b * g_inv for b in invariant_subgroup})
        fd_subgroups[site_group].append(orbit_repr)
        # Go from 'orbit_repr' to 'uc_site' to the original site.
        reverse_map[orbit_repr] = t_original * g_inv

    # Convert output to real sites
    fd_subgroups = {grp: tuple(it.starmap(lat, tags))
                    for grp, tags in fd_subgroups.items()}
    reverse_map = {lat(*tag): g for tag, g in reverse_map.items()}

    return fd_subgroups, reverse_map


def to_fd(site, fd):
    """Bring 'site' to the fundamental domain of 'sym'.

    Parameters
    ----------
    site : Site
    fd : container of Sites
        Sites in the fundamental domain.

    Returns
    -------
    g : GroupElement
        Group element that brings 'fd_site' to 'site'.
        As 'sym.quotient_group' is sorted, it always returns the
        same coset representative.
    fd_site : Site
        Ancestor of 'site' in the fundamental domain:
        ``site = act(g, fd_site)``.
    """
    fd = set(fd)
    lat, tag = site
    if not all(lat == fds.family for fds in fd):
        raise TypeError("'site' and 'fd' must belong to the same lattice.")
    fd = set(s.tag for s in fd)

    for r in lat.sym.quotient_group:
        t, fd_tag = lat.to_unit_cell(lat.act(r, tag))
        if fd_tag in fd:
            # 'fd_site' *is* image of 'site' in fundamental domain
            return r.inv() * t, lat(*fd_tag)
    else:
        raise ValueError('Tag {} has no images in the fundamental domain of {}'
                         .format(tag, lat))


# TODO: Modify this to work with sites from different lattices lattice, and a
#       symmetry that is a subgroup of the lattice symmetry. This should just
#       require the appropriate 'fd_subgroups' from the to and from lattices.

def fundamental_hoppings(hoppings, fd_subgroups, is_hermitian=True):
    """Return the minimal set that can generate 'hoppings' by applying 'sym'.

    Parameters
    ----------
    hoppings : sequence of (Site, Site)
        Sites must belong to the same family.
    fd_subgroups : dict: frozenset of GroupElements -> tuple of Sites
        The first return value of 'lat.fundamental_domain()' where 'lat'
        is the Lattice to which the sites of 'hopping' belong.
    is_hermitian: bool, default: True
        If True, treat the links as undirected and calculate reverse
        hopping by hermitian adjoint, hoppings are only expected and
        returned in one direction.

    Returns
    -------
    fd_hoppings : set of (Site, Site, GroupElement)
        Both sites are in the fundamental domain, and the group element is
        applied to the second site to get the true hopping.
    reverse_mapping : dict : (Site, Site, GroupElement) -> (GroupElement, bool)
        A map from fundamental hoppings to an operation that takes the hopping
        to the one originally specified by the user. The operation is specified
        as a group element and a flag that is true if the hopping should be
        Hermitian conjugated.  If several original hoppings map to the same
        fundamental one, then the one that appeared first in 'sites' is chosen.
    """
    fd_sites = set(it.chain.from_iterable(fd_subgroups.values()))
    fd_tags = set(site.tag for site in fd_sites)

    fams = set(it.chain.from_iterable((s1.family, s2.family)
               for s1, s2 in hoppings))
    fams |= {s.family for s in fd_sites}
    if len(fams) != 1:
        raise TypeError('All sites must belong to the same lattice.')
    lat = next(iter(fams))
    sym = lat.sym

    def to_uc(tag):
        return lat.to_unit_cell(tag)[1]

    hoppings = [(s1.tag, s2.tag) for s1, s2 in hoppings]

    def score(link):
        to, frm = link
        return (to in fd_tags, frm in fd_tags, frm == to_uc(frm), *to, *frm)

    # Return 'link' translated so that the first site is in the unit cell.
    def link_to_uc(link):
        to, frm = link
        t, to_prime = lat.to_unit_cell(to)
        frm_prime = lat.act(t.inv(), frm)
        return t, (to_prime, frm_prime)

    def link_act(g, link):
        a, b = link
        return lat.act(g, a), lat.act(g, b)

    # Move all hoppings to translational unit cell
    # Move all sites to translational unit cell, eliminating duplicates and
    # retaining the group element that brings the UC site to the site specified
    # by the user.
    uc_links = _take_first_seen(map(link_to_uc, hoppings), key=lambda r: r[1])
    reverse_map = {}
    fd_hoppings = set()

    # Keep only hoppings that are not related to any others by symmetry
    seen = set()
    for t_original, link in uc_links:
        if link in seen:
            continue
        orbit_repr = link
        orbit_transform = (sym.id, False)
        # Iterate over all symmetry images
        for r in sym.quotient_group:
            t, candidate = link_to_uc(link_act(r, link))
            g = t.inv() * r
            if score(candidate) > score(orbit_repr):
                # Keep if better than current representative
                orbit_repr = candidate
                orbit_transform = (g, False)
            seen.add(candidate)
            # If hermitian, bonds are undirected; reverse is also a candidate
            if is_hermitian:
                t, candidate = link_to_uc(reversed(candidate))
                g = t.inv() * r
                if score(candidate) > score(orbit_repr):
                    orbit_repr = candidate
                    orbit_transform = (g, True)
                seen.add(candidate)

        # Convert the hopping to canonical format.
        to, frm = orbit_repr
        h, frm = to_fd(lat(*frm), fd_sites)
        frm = frm.tag

        orbit_repr = (to, frm, h)
        g, conjugate = orbit_transform

        fd_hoppings.add(orbit_repr)
        reverse_map[orbit_repr] = (t_original * g.inv(), conjugate)

    # Convert output to real sites
    fd_hoppings = {(lat(*a), lat(*b), g) for a, b, g in fd_hoppings}
    reverse_map = {(lat(*a), lat(*b), g): h
                   for (a, b, g), h in reverse_map.items()}

    return fd_hoppings, reverse_map


# Tests


def test_basic():
    from pytest import raises

    I = (np.eye(2),)

    symmorphic_groups = (
        Symmetry.p1, Symmetry.p2, Symmetry.pm, Symmetry.cm, Symmetry.pmm,
        Symmetry.cmm, Symmetry.p4, Symmetry.p4m,)
        # Don't test hexagonal group for now
        # Symmetry.p3, Symmetry.p3m1, Symmetry.p31m, Symmetry.p6, Symmetry.p6m)

    nonsymmorphic_groups = (
        Symmetry.pg, Symmetry.pmg, Symmetry.pgg, Symmetry.p4g)

    point_groups = symmorphic_groups + nonsymmorphic_groups

    ### test regular construction
    for ptg in point_groups:
        sym = Symmetry(point_group=ptg)
        sym.reciprocal()

    # lattice vectors commensurate with point group
    # TODO: systematically test for all point groups that work with
    #       lattices with lattice vectors of different lengths
    for pg in (Symmetry.p2, Symmetry.pmm):
        Symmetry(point_group=pg, translations=[[1, 0], [0, 2.3]])

    ### test for invalid input
    # TODO: consistently test for lattices not commensurate with
    #       the provided point group
    bad_point_group = Symmetry.p4m[1:]  # remove identity element
    raises(ValueError, Symmetry, point_group=bad_point_group)
    bad_point_group = Symmetry.p4m + I  # twice the identity element
    raises(ValueError, Symmetry, point_group=bad_point_group)
    # lattice vectors not commensurate with point group
    # for pg in (Symmetry.p4m, Symmetry.p4):
    #     raises(ValueError, Symmetry, point_group=pg, tx=1, ty=2)

    ### test inverse
    sym = Symmetry()
    for pg in sym.quotient_group:
        assert pg.inv() in sym
        assert pg.inv() * pg == sym.id
        assert pg * pg.inv() == sym.id
        pg = GroupElement(sym, [1, 2], pg.r)
        assert pg.inv() * pg == sym.id
        assert pg * pg.inv() == sym.id

    ### test powers
    sym = Symmetry()
    for g in sym.quotient_group:
        assert g**-1 == g.inv()
        assert g**0 == sym.id
        for i in range(1, 5):
            ft.reduce(sym.mul, [g] * i) == g**i


def test_fundamental_domain():
    from pytest import raises

    I = (np.eye(2),)
    point_groups = (Symmetry.p1, Symmetry.p2, Symmetry.p4,
                    Symmetry.pmm, Symmetry.p4m)

    ### test unit cell construction
    tx_unit_cell = [(2, (0, 1)), (3, (-1, 0, 1)), (4, (-1, 0, 1, 2))]
    for div, uc in tx_unit_cell:
        lat = Lattice(Symmetry(), div=div)
        uc_tags = set(tuple(s.tag) for s in lat.unit_cell())
        assert uc_tags == set(it.product(uc, repeat=2))

    lat = Lattice(Symmetry(), div=6, lattice=np.diag([2, 3]))
    uc_tags = set(tuple(s.tag) for s in lat.unit_cell())
    assert uc_tags == set(it.product((-1, 0, 1), (0, 1)))

    # TODO: add tests for lattices that use vectors other than
    #       the scaled symmetry vectors

    ### test fundamental domain construction
    syms = [Symmetry(point_group=g) for g in point_groups]
    rot_lats = [Lattice(sym) for sym in syms]
    extended_lats = [Lattice(sym, div=t)
                     for t, sym in it.product((3, 4), syms)]
    rect_lats = [
        Lattice(Symmetry(point_group=g), div=12, lattice=np.diag([4, 3]))
        for g in (Symmetry.p2, Symmetry.pmm)]

    all_lats = rot_lats + extended_lats + rect_lats

    # check that attached subgroup really is a group, and that sites
    # really are invariant under these
    for lat in all_lats:
        fd_subgroups, _ = fundamental_domain(lat)
        for subgroup, sites in fd_subgroups.items():
            assert all(g1 * g2 in subgroup
                       for g1, g2 in it.product(subgroup, subgroup))
            assert all(all(lat.act(g, site.tag) == site.tag for g in subgroup)
                       for site in sites)
            # check that site at the centre is invariant under full point group
            if lat(0, 0) in sites:
                assert subgroup == set(lat.sym.quotient_group)

    # check fundamental domain for specific cases

    pg = Symmetry.p4m
    sym = Symmetry(point_group=pg)
    zv = ta.zeros(2, int)
    pg_elements = [
        ta.array(np.eye(2)),
        ta.array([[0, -1], [1, 0]]),   # anticlockwise π/2 rotation
        ta.array([[1, 0], [0, -1]]),   # mirror in |
        ta.array([[-1, 0], [0, 1]]),   # mirror in --
        ta.array([[0, 1], [1, 0]]),   # mirror in /
        ta.array([[0, -1], [-1, 0]]),   # mirror in \
    ]

    # point group elements look the same regardless of translational part
    e, c4, sx, sy, sxy, syx = [GroupElement(sym, zv, (zv, r))
                               for r in pg_elements]

    lat = Lattice(sym, div=2)
    tx = GroupElement(sym, [1, 0], sym.id.r)
    ty = GroupElement(sym, [0, 1], sym.id.r)
    fd_subgroups, _ = fundamental_domain(lat)
    fd_subgroups = {k: [tuple(s.tag) for s in v]
                    for k, v in fd_subgroups.items()}
    assert fd_subgroups[frozenset(sym.quotient_group)] == [(0, 0)]
    assert fd_subgroups[frozenset([e, tx * c4**2, sx, tx * sy])] == [(1, 0)]
    sg = frozenset([e, tx * c4, tx * ty * c4**2, ty * c4**3,
                    ty * sx, tx * sy, sxy, tx * ty * syx])
    assert fd_subgroups[sg] == [(1, 1)]

    lat = Lattice(sym, div=3)
    fd_subgroups, _ = fundamental_domain(lat)
    fd_subgroups = {k: [tuple(s.tag) for s in v]
                    for k, v in fd_subgroups.items()}
    assert fd_subgroups[frozenset(sym.quotient_group)] == [(0, 0)]
    assert fd_subgroups[frozenset([e, sx])] == [(1, 0)]
    assert fd_subgroups[frozenset([e, sxy])] == [(1, 1)]

    sym = Symmetry(point_group=Symmetry.pmm)
    lat = Lattice(sym, div=12,
                  lattice=np.diag([4, 3]))
    fd_subgroups, _ = fundamental_domain(lat)
    fd_subgroups = {k: [tuple(s.tag) for s in v]
                    for k, v in fd_subgroups.items()}
    tx = GroupElement(sym, [1, 0], sym.id.r)
    ty = GroupElement(sym, [0, 1], sym.id.r)
    assert fd_subgroups[frozenset(sym.quotient_group)] == [(0, 0)]
    assert fd_subgroups[frozenset([e, sx])] == [(1, 0)]
    assert fd_subgroups[frozenset([e, sy])] == [(0, 1)]
    assert fd_subgroups[frozenset([e])] == [(1, 1)]
    assert fd_subgroups[frozenset([e, ty * c4**2, ty * sx, sy])] == [(0, 2)]
    assert fd_subgroups[frozenset([e, ty * sx])] == [(1, 2)]

    sym = Symmetry(point_group=Symmetry.p4)
    lat = Lattice(sym, div=3)
    fd_subgroups, _ = fundamental_domain(lat)
    fd_subgroups = {k: [tuple(s.tag) for s in v]
                    for k, v in fd_subgroups.items()}
    assert fd_subgroups[frozenset(sym.quotient_group)] == [(0, 0)]
    assert set(fd_subgroups[frozenset([e])]) == {(1, 1), (1, 0)}

    sym = Symmetry(point_group=Symmetry.p4)
    lat = Lattice(sym, div=2)
    fd_subgroups, _ = fundamental_domain(lat)
    fd_subgroups = {k: [tuple(s.tag) for s in v]
                    for k, v in fd_subgroups.items()}
    tx = GroupElement(sym, [1, 0], sym.id.r)
    ty = GroupElement(sym, [0, 1], sym.id.r)
    assert fd_subgroups[frozenset(sym.quotient_group)] == [(0, 0)]
    assert fd_subgroups[frozenset([e, tx * c4**2])] == [(1, 0)]
    sg = frozenset([e, tx * c4, tx * ty * c4**2, ty * c4**3])
    assert fd_subgroups[sg] == [(1, 1)]


def test_fundamental_hoppings():
    # Test that numbert of sites and links is consrved for all wp groups


    # Test rectangular groups
    wpgroups = [Symmetry.p1, Symmetry.p2, Symmetry.pm,
                Symmetry.pg, Symmetry.cm, Symmetry.pmm,
                Symmetry.pmg, Symmetry.pgg, Symmetry.cmm,
                Symmetry.p4, Symmetry.p4m, Symmetry.p4g,
                ]

    # 1st nearest neighbors
    first_kinds = [(1, 0), (-1, 0), (0, 1), (0, -1)]
    # 2nd neighbors
    second_kinds = [(1, 1), (-1, 1), (1, -1), (-1, -1)]

    def which_subgroup(fd_subgroups, site):
        for sg, sites in fd_subgroups.items():
            if site in sites:
                return sg
        else:
            raise ValueError('{} is not in fundamental domain'
                             .format(site))

    def _hopping_group(fd_subgroups, link, is_directed=False):
        a, b, g = link
        sg_a = which_subgroup(fd_subgroups, a)
        sg_b = which_subgroup(fd_subgroups, b)

        hopping_grp, exchange_set = hopping_group(sg_a, sg_b, g)
        return hopping_grp if (is_directed or a !=b) else hopping_grp | exchange_set

    for wp in wpgroups:
        point_group = wp
        sym = Symmetry(point_group=wp)
        div = 2
        lat = Lattice(sym, div=div)
        # Test fundamental domain
        fd_subgroups, reverse_map = fundamental_domain(lat)
        # Symmetry group element numbers add up
        n_elements = sum(len(sym.quotient_group)/len(sg) * len(sites)
                         for sg, sites in fd_subgroups.items())
        assert n_elements == div * div

        # Test fd_hoppings
        uc_sites = lat.unit_cell()
        # 1st nearest neighbors
        links = [(site, lat(*(site.tag + k)))
                 for site in uc_sites for k in first_kinds]
        # 1st and 2nd nearest neighbors
        links += [(site, lat(*(site.tag + k)))
                  for site in uc_sites for k in second_kinds]

        # Test hermitian case
        fd_hoppings, _ = fundamental_hoppings(links, fd_subgroups,
                                              is_hermitian=True)
        fd_links = fd_hoppings
        # Symmetry group element numbers add up
        nlinks = len(first_kinds + second_kinds) / 2 * div * div

        hop_sg = ft.partial(_hopping_group, fd_subgroups,
                            is_directed=False)
        nlinks2 = sum(len(sym.quotient_group) / len(hop_sg(link))
                      for link in fd_links)
        assert nlinks2 == nlinks

        # Test non-hermitian case
        fd_hoppings, _ = fundamental_hoppings(links, fd_subgroups,
                                              is_hermitian=False)
        fd_links = fd_hoppings
        # Symmetry group element numbers add up
        hop_sg = ft.partial(_hopping_group, fd_subgroups,
                            is_directed=True)
        nlinks2 = sum(len(sym.quotient_group) / len(hop_sg(link))
                      for link in fd_links)
        assert nlinks2 == nlinks * 2

    # Test hexagonal groups
    wpgroups = [Symmetry.p3, Symmetry.p3m1, Symmetry.p31m,
                Symmetry.p6, Symmetry.p6m]
    # 1st nearest neighbors
    first_kinds = [(1, 0), (-1, 0), (0, 1), (0, -1), (1, 1), (-1, -1)]
    bas = ta.array([[1, 0], [-0.5, 0.5*(3**0.5)]])

    for wp in wpgroups:
        point_group = wp
        sym = Symmetry(translations=bas, point_group=point_group)
        div = div
        lat = Lattice(sym, div=div)
        # Test fundamental domain
        fd_subgroups, _ = fundamental_domain(lat)
        # Symmetry group element numbers add up
        n_elements = sum(len(sym.quotient_group)/len(sg) * len(sites)
                         for sg, sites in fd_subgroups.items())
        assert n_elements == div * div

        # Test fd_hoppings
        uc_sites = lat.unit_cell()
        # 1st nearest neighbors
        links = [(site, lat(*(site.tag + k)))
                 for site in uc_sites for k in first_kinds]

        # Test hermitian case
        fd_hoppings, _ = fundamental_hoppings(links, fd_subgroups,
                                              is_hermitian=True)
        fd_links = fd_hoppings
        # Symmetry group element numbers add up
        nlinks = len(first_kinds) / 2 * div * div

        hop_sg = ft.partial(_hopping_group, fd_subgroups,
                            is_directed=False)
        nlinks2 = sum(len(sym.quotient_group) / len(hop_sg(link))
                      for link in fd_links)
        assert nlinks2 == nlinks

        # Test non-hermitian case
        fd_hoppings, _ = fundamental_hoppings(links, fd_subgroups,
                                              is_hermitian=False)
        fd_links = fd_hoppings
        # Symmetry group element numbers add up
        hop_sg = ft.partial(_hopping_group, fd_subgroups,
                            is_directed=True)
        nlinks2 = sum(len(sym.quotient_group) / len(hop_sg(link))
                      for link in fd_links)
        assert nlinks2 == nlinks * 2


if __name__ == '__main__':
    test_basic()
    test_fundamental_domain()
    test_fundamental_hoppings()
