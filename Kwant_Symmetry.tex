\documentclass[aps,prb,superscriptaddress,notitlepage]{revtex4-1}
\usepackage{amsmath}
\renewcommand{\topfraction}{0.85}
\renewcommand{\textfraction}{0.1}
\renewcommand{\floatpagefraction}{0.75}
\usepackage[pdftex]{color}
\usepackage{epsfig}
\usepackage{graphicx}% Include figure files
%\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage{amssymb}%
\usepackage{textcomp}
\usepackage{gensymb}
\usepackage{bbm}

\newcommand{\ket}[1]{\left| #1 \right\rangle}
\newcommand{\bra}[1]{\left\langle #1 \right|}
\newcommand{\pars}[1]{\left( #1 \right)}
\newcommand{\brac}[1]{\left\{ #1 \right\}}
\newcommand{\eqn}[1]{\begin{equation} #1 \end{equation}}
\newcommand{\bA}{\mathbf{A}}
\newcommand{\bB}{\mathbf{B}}
\newcommand{\br}{\mathbf{r}}
\newcommand{\bR}{\mathbf{R}}
\newcommand{\bL}{\mathbf{L}}
\newcommand{\bk}{\mathbf{k}}
\newcommand{\bv}{\mathbf{v}}
\newcommand{\bM}{\mathbf{M}}
\newcommand{\bp}{\mathbf{p}}
\newcommand{\cH}{\mathcal{H}}
\newcommand{\id}{\mathbbm{1}}

\begin{document}
\bibliographystyle{apsrev4-1}
\title{Kwant symmetry notes}
\author{D{\'a}niel Varjas}
\begin{abstract}
We detail symmetry considerations about tight-binding models, with emphasis on the proposed symmetry module of Kwant.
\end{abstract}
\maketitle

\section{Introduction}

Our goal is to implement a low-level system in Kwant that is capable of handling arbitrary symmetries of tight-binding models. As the low level system operates at an abstract level, where the physical system is reduced to nodes and links of a graph, without any explicit real-space structure, it is necessary to treat the symmetries at a similarly abstract level as well. 

We assume the system is only specified in the fundamental domain of the full space group $SG$ (denoted $FD(SG)$), which defines the rest of the system by applications of the symmetry elements. This approach eliminates (some of) the redundancy in the defining data and (to some extent) guarantees that the defined system is indeed symmetric. However, it necessitates ``expansion'' of the system to the fundamental domain of the translation subgroup $FD(T)$ in order to carry out band structure calculations. This step is our main concern in the following.

\section{Structure of the symmetry group}

We assume that the symmetry group $G$ has the following structure:
\eqn{
G = SG \times  U
}
where $SG$ is the space group, with $T\trianglelefteq G$ an abelian normal subgroup (ANSG) of ``translations'' (in fact it can also contain rotation subgroups), $P = SG/T$ is the ``point group'' and $U$ is the subgroup of on-site symmetries. We assume here that elements of $U$ leave all sites invariant and commute with elements of $SG$. This may not necessarily be the case for every ``on-site symmetry'' (for example for sublattice symmetry in graphene), but such cases can also be treated by incorporating such symmetry elements in $P$. Every symmetry element can uniquely be written as
\eqn{
g = t p u
}
with $p$ a representative of a coset in $P$ (we assume the coset representatives are fixed and the group unit $e$ is a representative), $t\in T$ and $u\in U$. This ordering convention ($p$ acts before $t$, $p$ is a right coset representative) is common in the crystallography literature, and makes the translation part explicit. Transformation to the opposite convention is straightforward. Product of two group elements has the form
\eqn{
g g' = tpu t'p'u' = t (p t' p^{-1}) p p ' u u' = [t p(t') T(p p')] [(p p')\bmod T] [u u']
}
where we defined the action of $p$ on $t$ as $p(t) = p t p^{-1}$, the coset representative in $P$ corresponding to $g\in SG$ as $g \bmod T$ and the purely translational part of $g$ as $T(g)$ such that $g = T(g) (g\bmod T)$. (This is necessary because the product of two coset representatives is in general not a coset representative.) Note that the exact form of the rule depends on our choice for the canonical ordering. As the subgroup $U$ enters our following discussions trivially (commutes with everything) we omit it. 
%Now the product rule for $g,g' \in SG$ can be written as
%\begin{eqnarray}
%T(g g') &=& T(g) p(T(g')) T(P(g)P(g')) \\
%P(g g') &=& P(P(g) P(g')).\\
%\end{eqnarray}
To specify the group structure of $SG$ we need:
\begin{itemize}
\item The number of generators of $T$. Some generators of $T$ might be cyclic, i.e. have a finite order, which also needs to be specified. Elements of $T$ can be represented as vectors of integers, the product of two elements is the sum of the two vectors, modulo the finite orders in the appropriate components. Inverse is multiplication by $-1$.
\item The action of every coset representative in $P$ on every generator of $T$. This can be represented as an integer matrix for each coset representative.
\item Product rule among the coset representatives, that produces the result in the canonical form of $pp' = T(pp') (pp' \bmod T)$. Even though it follows from the product rule, in practice a similar inverse function is also needed $p^{-1} = T(p^{-1}) (p^{-1}\bmod T)$. The operation $pp'\to pp' \bmod T$ (multiplication ignoring the translaion part) can be regarded as the product rule of $P = SG/T$, under this (and the $\bmod  \;T$ inverse) the coset representatives form a group.
\end{itemize}

\section{Symmetry action on the nodes}

If we want to apply symmetry considerations to a given lattice, we need to know how its sites transform. In crystallography, lattice sites have different Wyckoff-positions, that determines which $SG$ elements leave it invariant, this is the ``point group of site'' . Similarly, in our abstract graph, we need to specify the ``point group of node $i$'' 
\eqn{
N_i = \brac{g\in SG : gi = i}
}
where $gi$ is the symmetry image of node $i$ under $g$. We take the active point of view, $gi$ physically corresponds to the site where site $i$ is moved by application of the symmetry operation $g$. As for now, $gi$ is an abstract node, which, unless $g\in N_i$, is different from $i$. $N_i \leq SG$ is a finite closed subgroup of $SG$, generally not a set of coset representatives of $P$. There may be multiple distinct nodes sharing the same type of Wyckoff position, thus the same $N_i$, so this property should be defined \texttt{node\_range}-wise for efficiency.

All the unique images of node $i$ are labeled by the left cosets $gN_i$. This is seen, by considering that application of any $n\in N_i$ leaves $i$ invariant, so the image under $g$ or $gn$ should be identical. The image of a node $gi$ under $h\in SG$ is $hgi$. $gi$ is invariant under $h$ iff $hg$ belongs to the same coset as $g$, meaning $\exists n\in N_i$ such that $gn = hg$. This is equivalent to $h = g n g^{-1}$ with $n\in N_i$. So the point group of $gi$ is 
\eqn{
N_{gi} = g N_i g^{-1}.
}
This still contains an infinite number of cosets if $T$ is infinite, we need to separate the translational part. As $T$ is normal in $SG$, $T N_i = \langle T, N_i \rangle$ (the group genetated by $T$ and $N_i$) is the set of all symmetries that leave $i$ invariant up to translations. It has a finite number of cosets that are indexed by elements of $P$. According to the group theoretical correspondence theorem, there is a one-to-one correspondence between subgroups of a group ($SG$) that contain a normal subgroup ($T$) and the subgroups of the quotient ($P=SG/T$). Applying this to $T N_i \geq T$ we find that it exactly corresponds to a subgroup $P_i \leq P$, and its cosets in $SG$ correspond to the cosets of $P_i$ in $P$. 

Less abstractly, we take point group elements corresponding to the elements of $N_i$, which form a subgroup $P_i \leq P$. (We obtain $P_i$ from $N_i$ by ignoring all the translation parts. When applying the group multiplication ignoring the translational part, these form a subgroup). We form the corresponding cosets in $P$ (again ignoring the translational parts). From each coset we choose a representative $r$ forming the set of representatives $R_i$ (such that $r$ is one of the $P$ representatives and $e\in R_i$), and also form the cosets $\brac{r N_i}_{r\in R_i}$ (these \emph{include} the appropriate translational parts). Each of these cosets corresponds to a unique image of $i$, $ri$, and allows indexing these images by $r\in R_i$. This set of images define $FD(T)$ (the ``translational'' unit cell), as every other image of $i$ can be obtained by pure translations. Each element of $P$ (and $SG$) appears in exactly one of the above cosets modulo $T$. The set $R_i N_i = \bigcup_{r\in R_i} r N_i$ defines a set of representatives for $P$ that map node $i$ inside $FD(T)$, which in general differs in translations from the original set of representatives.
\begin{itemize}
\item Define $p_i (g) = \brac{p\in R_i N_i: p \bmod T = g \bmod T}$ as the group element in $R_i N_i$ that has the same point group part as $g$. 
\item Define $r_i (g) = \brac{r\in R_i : p_i(g) \in rN_i}$ as the coset representative of the coset $r N_i$ in which $p_i (g)$ appears.
\item Define $n_i (g) = r_i(g)^{-1} p_i(g)\in N_i$ as the element in the point group of the node that relates the above coset element to the coset representative.
\item Define $t_i(g) = g p_i(g)^{-1} \in T$, the explicit translation part of $g$ with the new $P$ representatives.
\end{itemize}
These functions can be implemented multiple ways. One is storing the sets $r N_i$ for every $r\in R_i$, such that $r$ is always the first element in the respective set. $p_i$ and $r_i$ can be evaluated by searching for and returning the element that has the same point group part, and the first element of its set respectively. Another way is explicitly storing a list that specifies for every $g\in P$ representative (these are indexed in a canonical way) the corresponding $p_i (g)$ and $r_i(g)$, that could be looked up by the index of the point group part of an arbitrary $g$. 

This allows for the unique expansion of any group element as
\eqn{
g = t_i(g) p_i(g) = t_i(g) r_i(g) n_i(g)
}

Every site related to $i$ can be uniquely indexed as $tri$ where $t\in T$ and $r\in R_i$ is one of the above coset representatives. ($N_i$ might include a cyclic subgroup of $T$, in this case this indexing is not unique. But it should not lead to confusion in the following if there are no unnecessary translations included in any of the defined hoppings.)

For later use it is useful to give a canonical decomposition of the action of an arbitrary $g\in SG$ on an  arbitrary site $tri$ in the above convention. We do this by pulling back the site to $FD(T)$ (the first UC), then to the $FD(SG)$, mapping to the image site and translating: $ tri \to ri \to i \to p_i(gr) i \to t_i(gr) p_i(gtr) i = gri $,
% or by pulling back the site to the FD, translating and mapping to the image site: $ ri \to i \to t'_i(gp)i \to t'_i(gp) p_i(gp) i = gri $,
\eqn{
g = t_i(gtr) p_i(gr) r^{-1} t^{-1} = t_i(gtr) r_i(gr) n_i(gr) r^{-1} t^{-1} 
%= p_i(gr) t'_i(gr) r^{-1}
}
where we used that $p_i$, $r_i$ and $n_i$ only depend on the argument modulo translations. This shows that the image of a site $tri$ is the site $r_i(gr) i$ in the UC translated by $t_i(gtr)$.

\section{Symmetry action on Hilbert spaces}

In the tight-binding formalism there is a finite dimensional Hilbert space on every node. We assume that every site in a given \texttt{node\_range}, besides having the same $N_i$, also has the same Hilbert space dimensionality and Hilbert space transformation properties, so all the following properties can also be treated \texttt{node\_range}-wise. First we keep everything general with a possibly infinite dimensional full Hilbert-space, only switching to Bloch functions in the end.

We denote the Hilbert-space of a node $i$ as $\cH_i$. Every node has a fixed set of basis states $\brac{\ket{l,i}}_{l=1..\dim\cH}\subset \cH_i$ which \emph{can be different} on symmetry related sites. We use the active view of symmetry again: a symmetry operation acting on a basis state maps it to a combination of basis states on the image site:
\begin{eqnarray}
\bra{k,gi} \pars{\hat{U}(g) \ket{l,i}} &=& U_{kl} (g,i),\\
\hat{U}(g) \ket{l,i} &=& \ket{k, gi} U_{kl} (g,i),\\
\hat{U}(g,i) &=& \ket{k, gi} U_{kl} (g,i) \bra{l,i} 
\end{eqnarray}
where we imply summation over repeated vector indices ($k$, $l$, etc). $\hat{U}(g)$ is the (anti)unitary symmetry representation on the full Hilbert space. $\hat{U}(g,i): \cH_i \to \cH_{gi}$ is its restriction to the Hilbert space of site $i$ and $U_{kl}(g,i)$ is its expansion in the local basis.

Acting on a state expanded in the basis $\ket{\psi} = \sum_{tikr} \psi_k(tri)\ket{k,tri}$ we find the group element's action on the expansion coefficients:
\eqn{
(\hat{U}(g)\psi)_k(tri) = U_{kl}(g,tri) \psi_l(g^{-1}tri).
}

These operators form a representation of the symmetry group:
\begin{eqnarray}
\hat{U}(gg') &=& \hat{U}(g)\hat{U}(g'),\\
\hat{U}(gg',i) &=& \hat{U}(g,g'i)\hat{U}(g',i).
\end{eqnarray}
In order to store antiunitary operators (such as time-reversal of particle-hole) as well as antisymmetries (operators that anticommute with the Hamiltonian, such as particle-hole or chiral symmetry) we need a triple $\brac{U, c, a}(g,i)$ where $U$ is a matrix, $c$ and $a$ are booleans. The product rule is:
\eqn{
\pars{\brac{U, c, a} \cdot \brac{U', c', a'}}_{kl} = \left\{ 
\begin{array}{ll}
\brac{U_{km} U'_{ml},c\&c' ,a\&a'} & \text{if }c=0\\
\brac{U_{km} (U'_{ml})^*,c\&c' ,a\&a'} & \text{if }c=1\\
\end{array}
\right.
}
where $\&$ is binary addition. With this product rule the representation satisfies
\eqn{
\brac{U, c, a}(gg',i) = \brac{U, c, a}(g,g'i)\cdot \brac{U, c, a}(g',i).
}
In the following we drop the booleans and only use $U$ for brevity, but keep this convention in mind.

To specify the symmetry action on the tight-binding model's Hilbert space we need for each \texttt{node\_range} $i$:
\begin{itemize}
\item Action of the point group of the node $U(n,i)$ for all $n\in N_i$. (In fact it is enough for only the generators of the group, but we should store them all.)
\item Action of every coset representative $U(r,i)$ for all $r\in R_i$. From this two, the actions of every coset element can be constructed.
\item Action of translation generators $U(t_d,i)$. We also assume that the action is the same on every symmetry related site $U(t,r i) = U(t,i)$ for all $t\in T$ and $r\in R_i$ (not perfectly sure whether this assumption always holds or if there is a way around it). For infinite translations this can be chosen to be the identity. For cyclic abelian normal subgroups, this may be nontrivial.
\item Action of on-site symmetry group elements $U(u,i)$ for all $u\in U$. As elements of $U$ leave all sites invariant, this data specifies the group structure of $U$. $U$ might also contain a continuous Lie-group (for example $SU(2)$ spin rotations), in that case it is sufficient to store the action of the skew-hermitian generators only (e.g. $i\sigma_i$). The on-site nature of these symmetries means that the operator exponential with real coefficients is also a unitary symmetry.
\end{itemize}

From these the symmetry action of an arbitrary $g\in SG$ on an arbitrary site $tri$ can be expressed as:
\begin{eqnarray}
U (g, tri) &=& U \pars{t_i(gtr)  p_i(gr) r^{-1} t^{-1} ,tri} = \\
&=& U\pars{t_i(gtr),i} U(r_i(gr),i) U(n_i(gr),i) U(r,i)^{-1} U(t,i)^{-1}
\end{eqnarray}
where we used that $U(g^{-1},gi) = U(g,i)^{-1}$ and $U\pars{t,ri} = U\pars{t,i}$. Here we wrote the action of $\hat{U(g,tri)}: \cH_{tri} \to \cH_{t_i(gtr)r_i(gr) i}$ as $\cH_{tri}\to\cH_{ri}\to\cH_{i}\to \cH_{i} \to \cH_{r_i(gr) i} \to \cH_{t_i(gtr) r_i(gr) i}$
 Alternatively, assuming we know the action of every coset element we can write the simpler expression:
\eqn{
U (g, tri) = U\pars{t_i(gtr),i} U(p_i(gr),i) U(r,i)^{-1} U(t,i)^{-1}.
}

Switching to $\bk$-space, we fix a representation of the translation subgroup which allows the reduction of real space to $FD(T)$. We denote the representation $\rho_{\bk}(t) = e^{2\pi i\bk\mathbf{t}}$ where $\mathbf{t}$ is the vector of integers in expansion of $t\in T$ in powers of the translation generators $\brac{t_d}$. For an infinite translation $t_d$ the corresponding component is real $k_d\in [0,1)$. For a cyclic ANSG of order $L_d$ such that $t_d^{L_d} = e$ the allowed values of $\bk_d$ are defined by $\hat{U}(t_d,i)^{L_d} = e^{2\pi i\phi}\id$. We allow unit complex numbers other than $+1$ to be able to treat rotations of spin-$1/2$ systems ($\phi = 1/2$) or threading arbitrary magnetic flux through the ``hole'' of a cylinder ($\phi$ is the flux in units of the flux quantum). The allowed representations have $\bk_d = \frac{1}{L_d} \pars{n+\phi}$ with $n\in\mathbb{Z}_{L_d}$.

Demanding $\hat{U}(t) \ket{\psi} = \rho_{\bk}(t)\ket{\psi}$ fixes the wave functions in every translated unit cell in terms of the wave function in the first UC (note that this convention has an opposite sign compared to the usual definition of $\bk$, the phase of the wave function winds backwards when moving in the direction of $\bk$):
\eqn{\label{eqn:BlochWF}
\psi_k(tri) = \rho_{\bk}(t)^{-1} U_{kl}(t,i) \psi_l(ri) = \rho_{\bk}(t)^{-1} U_{kl}(t,i) u_{\bk l}(ri) .
}
This allows restricting the Hilbert space of the full system to the Hilbert space of the UC at a fixed $\bk$, $\cH_{\bk}$. We define Bloch wave function as the restriction of the wave function to the first UC $\ket{u_{\bk}} = \ket{\psi}|_{\text{UC}} = \sum_{ikr} \psi_l(ri) \ket{l,ri}\in \cH_{\bk}$, the expansion coefficients $u_{\bk l}(ri) = \psi_l(ri)$. For sites that are invariant under a cyclic ANSG ($tri=ri$), the above relation poses a nontrivial constraint on the wave function which has to be taken into account when solving.

The action on the Bloch functions is
\eqn{
(\hat{U}(g) u)_k(ri) = U_{kl}(g,ri) u_{\bk l}(r_i(g^{-1}r)i)
}
with
\eqn{
U (g, ri) = \rho_{\bk}(t_i(gr)) U(p_i(gr),i) U(r,i)^{-1}.
}
%
\section{Hamiltonian terms}
%
To fix the convention, we define Hamiltonian terms as the matrix elements with $i$ and $j$ arbitrary sites
\eqn{
H_{kl}(i,j) = \bra{k,i} \hat{H} \ket{l,j}.
}
Because of the hermiticity of $\hat{H}$ $H(i,j) = H(j,i)^{\dag}$ (we drop the local basis indices when it doesn't cause confusion). This restricts on-site terms $H(i,i)$ to be hermitian. By lexicographical ordering of sites, it is possible to only store terms $H(i,j)$ for $i\leq j$, making sure each hopping is only specified in one direction.

The symmetry restricts the matrix elements to be the same between symmetry related images of the states for any $g\in G$ (here we allow on-site symmetries for generality)
\eqn{
\bra{k,i} \hat{H} \ket{l,j} = \bra{k,i} \hat{U}(g')^{\dag} \hat{H} \hat{U}(g')\ket{l,j}
}
resulting in
\begin{eqnarray}
\hat{H} &=& \hat{U}(g')^{\dag} \hat{H} \hat{U}(g')\\
H(i,j) &=& U(g,i)^{\dag} H(gi,gj) U(g,j)
\end{eqnarray}
A minimal set of terms that define $\hat{H}$ have $i$ restricted to the FD and we write $j$ as $trj$ where $j$ is also restricted to the FD and $T\in t$, $r\in R_j$. We denote these as $H(i,j,tr) = H(i,trj)$ and only need to specify terms where $i\leq trj$. Any term can be expressed in terms of these as
\eqn{
H(tri,t'r'j) = \left\{ \begin{array}{ll}
U(tr,i) H(i,j,t_j((tr)^{-1} t'r')r_j(r^{-1} r')) U(tr,t'r'j) & \text{if } i\leq (tr)^{-1} t'r' j \\

U((t'r')^{-1},tr i)^{\dag} H(j,i,t_j((t'r')^{-1} tr)r_j((r')^{-1} r))^\dag U(t'r',j)^{\dag} & \text{if } i\geq (tr)^{-1} t'r' j

\end{array} \right. .
}

This convention still leaves nontrivial symmetry constraints, as terms are invariant under symmetries that leave the corresponding edge invariant. One way this can happen, is when both $i$ and $trj$ is invariant, for $g\in (N_{i} \cap N_{trj}) \cup U$:
\eqn{
H(i,j,tr) = U^{\dag}(g,i) H(i,j,tr) U(g,trj).
}
A common special case of the above is the case of on-site terms with $i=j$, $tr=e$, for any $g\in N_i \cup U$:
\eqn{
H(i,i,e) = U^{\dag}(g,i) H(i,i,e) U(g,i).
}
The other possibility is when the two ends of the edge are interchanged. This can only happen if the ends are symmetry related ($j=i$) and $gi=tri$ and $gtri=i$, meaning $g\in \pars{trN_i}\cap \pars{N_i (tr)^{-1}}$, which restricts
\eqn{
H(i,i,tr) = U^{\dag}(g,i) H(i,i,tr)^{\dag} U(g,tri).
}
The symmetry group of the edge (from now on ignoring the on-site part) can be written as
\eqn{
E_{i,trj} = \left\{ \begin{array}{ll}
N_{i} \cap \pars{tr N_{j} (tr)^{-1}} & \text{if } i\neq j \\

\pars{N_{i} \cap \pars{tr N_{i} (tr)^{-1}}}\cup\pars{\pars{trN_i}\cap \pars{N_i (tr)^{-1}}} & \text{if } i=j

\end{array} \right. .
}
which can be checked to form subgroups of $SG$. 

Similarly to the procedure with the node symmetry groups, we can generate the finite list of cosets of $T E_{i,trj}$ which enumerate all images of the edge unique up to translations. By choosing the coset representatives from $R_i N_i$ we ensure that all unique images of edge $(i,trj)$ have the image of $i$ within $FD(T)$. Let $R_{i,trj}$ denote the set of left coset representatives with respect to $T E_{i,trj}$ chosen from the above set. For each $r'\in R_{i,trj}$ there is a unique hopping in the $FD(T)$ picture:
\eqn{
H(I,J,t') = H\pars{r_i(r')i, r_j(r'tr)j, t_j(r'tr)} = U(r',i) H(i,j,tr) U(r',trj)^{\dag}
}
	where $I = r_i(r')i$ and $J = r_j(r'tr)j$ are canonically labeled sites of $FD(T)$ and $t' = t_j(r'tr)$ is a pure translation. This scheme guarantees that if originally every hopping to/from $FD(SG)$ was only defined once and in one direction, the same will be true for the new set of hoppings to/from $FD(T)$, as we generate all unique symmetry images up to translations, treating the reverse direction hopping as identical. To get all the terms, we need to add a new term $H(J,I,(t')^{-1}) = H(I,J,t')^{\dag}$ for every $I\neq J$ and $t\neq e$. (Note that for the new terms it is not necessarily true that $I\leq tJ$.)

An alternative, perhaps easier to implement method is to sum the contributions for every $r'\in R_i N_i$ to the term with $I = r_i(r')i$, $J = r_j(r'tr)j$ and $t' = t_j(r'tr)$:
\eqn{
H(I,J,t') \mathrel{+}= H\pars{r_i(r')i, r_j(r'tr)j, t_j(r'tr)} = \frac{1}{|E_{i,trj}|} U(r',i) H(i,j,tr) U(r',trj)^{\dag}.
}
By this method, the same term $H(I,J,t')$ might get multiple contributions and some of the contribution might go into the reverse hoppings $H(J,I,(t')^{-1})$. To get all the terms correctly, we need to symmetrize with respect to the reverse hopping contributions $H(I,J,t')\mathrel{+}=H(J,I,(t')^{-1})^{\dag}$ for all $I\neq J$ and $t\neq e$. Dividing by the number of duplicates $|E_{i,trj}|$ guarantees that the resulting Bloch Hamiltonian will be identical to the previous one. This scheme effectively symmetrizes the Hamiltonian, the result will be symmetric under $SG$ even if the original terms did not satisfy the above symmetry conditions.

To construct the Bloch Hamiltonian we restrict the symmetry group to $T$ and substitute the ansatz (\ref{eqn:BlochWF}) in the Schr{\"o}dinger equation $\hat{H}\ket{\psi} = E\ket{\psi}$ to get
\eqn{
\sum_{J m} H_{\bk}(I,J)_{lm} u_{\bk m}(J) = E_{\bk} u_{\bk l} (I)
}
with
\eqn{
H_{\bk}(I,J) = \sum_t H(I,J,t) \rho_{\bk}(t)^{-1} U(t,J).
}
Here the indices $I$ and $J$ are nodes of $FD(T)$ that can be uniquely indexed as $I = r i$ with $r\in R_i$ and $J = r j$ with $r\in R_j$. As shown above, all the terms $H(I,J,t)$ can be generated from the minimal set of $H(i,j,tr)$.

\bibliography{Kwant_symmetry}
\end{document}
 